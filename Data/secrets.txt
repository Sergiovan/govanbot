#private
	exports.secrets = {
		username: 'bot username goes here',
		password: 'bot password goes here',
		guardCode: 'bot steam guard goes here',
		profileId: 'bot profile id goes here',
		ownerId: 'bot owner id goes here',
		hmacSecret: 'random string used to encode usernames during export',
		SFD : 'bot superuser goes here',
		mainChat : 'main chat group goes here',
		betaChat : 'beta chat group goes here',
		botName : 'bot username goes here'
	};
	
	exports.synonyms = {
		'name': ["bot username here", "bot", "robot"],
		'leave' : ["fuck off", "leave", "bugger off", "go away", "chop chop", "piss off"],
		'number' : ["1","2","3","4","5","6","7","8","9","0"]
	};
	
    //Stuff for google drive
	exports.google = {
		CLIENT_ID : "Your Client Id",
		CLIENT_SECRET : "Your Secret Client Id",
		REFRESH_TOKEN : "A refresh token",
		PARENT_FOLDER_ID : 'The drive folder',
		LINK : "A link to the drive folder"
	};
    
    //Stuff for pastebin
    exports.pastebin = {
		username : 'Pastebin name',
		password : "Pastebin pass",
		apikey : 'Your pastebin dev api key',
		userkey : 'Your pastebin user api key'
	};
    
    exports.keys = {
        youtube_dev_key : 'Your YT dev key'
    };