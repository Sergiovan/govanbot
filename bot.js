'use strict'; //Here we go again...

//TODO More error types

/* Modules here */
const fs = require('fs');
const util = require('util');
const path = require('path');
const crypto = require('crypto');
const steam = require('steam');
const youtube = require('youtube-api');
const request = require('request');
const mkdirp = require('mkdirp');
const validurl = require('valid-url');
const colors = require('colors/safe');
const imgur = require('imgur');
const server = require('socket.io');
const glob = require('glob');
const twitter = require('twitter');
const cron = require('cron');
const extIP = require('external-ip');
const geoip = require('geoip-lite');
const random_seed = require('random-seed');
const clone = require('clone');
const async = require('async');
const escapeRegex = require('escape-string-regexp');

/* Own modules here */
const secrets = require('./secrets.js');
let S = require('./strings.js');

/* Global variables here */
colors.enabled = true; //Set it here so they're activated the whole time
let beta = !!process.argv[2]; //Set it here because it may affect startup
const GUI_VERSION = 1.03;
const GOVAN_VERSION = '4.2';
const GOVAN_SUBVERSION = 'a';

/** @type {Users|User[]} */
let users = {obj: {}};
/** @type {Commander} */
let comm = {};
/** @type {Server} */
let io = {
    broadcast(){},
    messageSuperUsers(){},
    messageAdmin(){}
};

let stclient = new steam.SteamClient();
let stuser = new steam.SteamUser(stclient);
let bot = new steam.SteamFriends(stclient);

const sentryPath = path.join('data', 'sentry');

const steamLinkRegex = /(?:(?:https?|steam)\:\/\/|www\.).+/;

/* Third party keys and shenanigans */
youtube.authenticate({
    type: "key",
    key: secrets.keys.youtube_dev_key
});

//Imgur here
imgur.setClientId(secrets.imgur.client);

//Twitters here
const twclient = new twitter({
    consumer_key: secrets.twitter.consumer_key,
    consumer_secret: secrets.twitter.consumer_secret,
    access_token_key: secrets.twitter.token_key,
    access_token_secret: secrets.twitter.token_secret
});

/* Modifying bot */

stclient.login = function(){
    let userName, password, code, sentry, sentrySha;

    try{
        sentry = fs.readFileSync(sentryPath);
        sentrySha = makeSha(sentry);
    }catch(err){
        sentry = null;
        sentrySha = null;
    }

    if(!beta){
        userName = secrets.steam.username;
        password = secrets.steam.password;
        code = secrets.steam.guardCode;
    }else{
        userName = secrets.steam.betaname;
        password = secrets.steam.betapass;
        code = secrets.steam.betaCode;
    }

    if(!sentrySha && !code){
        stuser.logOn({account_name: userName, password: password});
    }else if(!sentrySha){
        stuser.logOn({account_name: userName, password: password, sha_sentryfile: sentrySha, auth_code: code});
    }else{
        stuser.logOn({account_name: userName, password: password, sha_sentryfile: sentrySha});
    }
};

bot._joinChat = bot.joinChat;
bot.joinChat = function(chatID){
    bot._joinChat(chatID);
};
bot._leaveChat = bot.leaveChat;
bot.leaveChat = function(chat = null){
    if(!chat){
        chat = bot.botData.chat;
    }
    bot._leaveChat(chat);

    Logger.status('Left chat');
};
bot._sendMessage = bot.sendMessage;
bot.sendMessage = function(steamID, message, secret = false, superSecret = false){
    if(!steamID){
        return;
    }
    if(bot.botData.mode === BotData.modes.dalek){
        message = message.toUpperCase();
    }
    if(isUserID(steamID)){
        let failures = '';
        if(!users[steamID]){
            failures += 'Failed'
        }else if(users[steamID].subscriptions.get('silence') || bot.botData.mode === BotData.modes.silent){
            failures += failures ? ', Silenced' : 'Silenced';
        }else{
            if(users[steamID].curses.blind){
                message = message.garble();
            }
            if(bot.botData.mode !== BotData.modes.silent){
                try{
                    bot._sendMessage(steamID, message);
                }catch(anything){ //Known to fail after steam does weird things
                    /* TODO investigate random failures */
                    Logger.error(anything);
                    exit();
                }
            }
        }
        let name = (superSecret ? '??????' : users[steamID].name);
        let printMessage = (secret ? '??????' : message);
        failures = failures ? ` (${failures})` : '';
        if(secret || superSecret){
            Logger.printMessage(colors.red(`I told ${name}: ${printMessage}`) + failures, Logger.guivan);
        }else{
            Logger.printMessage(colors.yellow(`I told ${name}: ${printMessage}`) + failures, Logger.guivan);
        }
    }else{
        if(bot.botData.mode !== BotData.modes.silent){
            try{
                bot._sendMessage(steamID, message);
            }catch(anything){ //Known to fail after steam does weird things
                /* TODO investigate random failures */
                Logger.error(anything);
                exit();
            }
        }
        Logger.log(colors.cyan(`I said: ${message}${bot.botData.mode === BotData.modes.silent ? ' (Silenced)': ''}`));
    }
};
bot.setGame = function(game){
    if(game){
        stuser.gamesPlayed({games_played: [{game_id: 753}, {game_id: 9759487592989982720, game_extra_info: game}]});
    }else{
        stuser.gamesPlayed({games_played: [{game_id: 753}]});
    }
};
bot._kick = bot.kick;
bot.kick = function(chatID, userID){
    if(bot.chatRooms[chatID][stclient.steamID].permissions & steam.EChatPermission.Kick){
        bot._kick(chatID, userID);
    }else if(chatID === secrets.steam.mainChat){
        bot._sendMessage('76561197998201250', `/kick ${userID}`);
    }else{
        Logger.debug("I don't have the power to do that ;-;");
    }
};
bot.botData = {};

//region Functions

/* Extending native objects. Forgive me */

if(!Array.prototype.randomElement){
    Array.prototype.randomElement = function(){
        if(!this.length){
            return null;
        }else{
            return this[Math.floor(Math.random() * this.length)];
        }
    };
    Object.defineProperties(Array.prototype, {
        randomElement: {enumerable: false}
    });
}

if(!Function.prototype.placeholder){
    Function._ = Symbol();

    Function.prototype.placeholder = function(thisArg, ...args){
        let self = this;
        return function wrapped(...callArgs){
            let pos = 0;
            let finalArgs = args.map(function(value){
                if(value === Function._){
                    return callArgs[pos++];
                }else{
                    return value;
                }
            });
            finalArgs.push(...(callArgs).slice(pos));
            self.call(thisArg, ...finalArgs);
        }
    };
    Object.defineProperties(Function.prototype, {
        placeholder: {enumerable: false}
    });
}

if(!String.prototype.garble){
    String.prototype.garble = function(){
        const mask = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\'
                .split('');
        return this.replace(/./g, function(x) {
            if(Math.random()*10 < 1){
                return x;
            }else{
                return mask.randomElement();
            }
        });
    };
    Object.defineProperties(String.prototype, {
        garble: {enumerable: false}
    });
}

const _ = Function._;

//region Functions

/** Starts up everything */
function init(){
    Logger.info('Starting...');

    io = new Server();

    /** @type {Users|User[]} */
    users = new Users();
    users._users.save();

    bot.botData = new BotData();

    comm = new Commander();

    //region Commands

    //region Group commands

    /* One-word commands */
    comm.addCommand('ping', ParseNode.noTree(),
    function(parsed, user, chatID){
        bot.sendMessage(chatID, 'Pong');
    })
    .document({
        general: "Will make the bot reply with !pong"
    });

    comm.addCommand('here', ParseNode.noTree(),
    function(parsed, user, chatID){
        let here = Object.keys(bot.chatRooms[chatID]).length;
        bot.sendMessage(chatID, `About ${here}${here !== 10 ? ' (10*)' : ''}`);
    })
    .document({
        general: 'Will tell you how many people there are in the chat'
    });

    comm.addCommand('random', ParseNode.noTree(),
    function(parsed, user, chatID){
        let here = Object.keys(bot.chatRooms[chatID]);
        let chosen = here.randomElement();
        chosen = users[chosen];
        let name = chosen.id === stclient.steamID ? 'myself' : chosen.banned ? 'that one guy' : chosen.name;
        bot.sendMessage(chatID, `I choose ${name}`);
    })
    .document({
        general: 'Will select a random person present in the chat'
    });

    comm.addCommand('mfw', ParseNode.noTree(),
    function(parsed, user, chatID){
        if(!bot.botData.imson){
            try{
                imgur.getAlbumInfo('gSvAX').then(function(json){
                    bot.botData.imson = json;
                    bot.sendMessage(chatID, json.data.images.randomElement().link);
                });
            }catch(err){
                Logger.info('Imgur decided to take a day off. Ok');
                bot.sendMessage(chatID, 'Imgur is having some troubles. Try again later');
            }
        }else{
            bot.sendMessage(chatID, bot.botData.imson.data.images.randomElement().link);
        }
    })
    .document({
        general: 'Will post a random picture from the mfw album (Maintained by Out_SiN)'
    });

    const possibilities = ["$yn$", "Maybe", "Probably", "Ask again later", "Only on $dow$",
        "Why should I know?", "Definitely $yn$", "Don't even think about it", "My bits say $yn$",
        "My console window is tingling. I'd say $yn$", "My source says yes", "My sources say no",
        "10*", "Only if your username starts with a consonant", "Don't worry. The answer will come with time",
        "$yn$. Wait... $yn$", "$yn$. Duh", "$yn$, obviously", "Interestingly enough, $yn$",
        "Unlikely", "You'll find the answer deep within your heart", "Ask google",
        "I'd say $yn$, but I'd be lying", "Since you asked so nicely, $yn$", "Clearly $yn$",
        "You open a log. Inside you find a piece of paper: \"$yn$\"", "Only if your username starts with a vowel",
        "Clearly $yn$", "Judging by the position of Neptune... $yn$", "Silly you, of course the answer is $yn$",
        "Open your eyes. $yn$", "A thousand times $yn$", "It's opposite day, so... $yn$"]; //Yes
    comm.addCommand('!magic8', ParseNode.noTree(),
    function(parsed, user, chatID){
        if(randInt(10000) < 2){
            bot.sendMessage(chatID, 'The 8-ball broke. Good job');
        }else{
            let result = possibilities.randomElement();
            result = result.replace(/\$(.*?)\$/g, function(match, p1, offset){
                let rett = '';
                if(p1 === 'yn'){
                    rett = ['Yes', 'No'].randomElement();
                }else if(p1 === 'dow'){
                    rett = ["Mondays", "Tuesdays", "Thursdays", "Wednesdays", "Fridays", "Saturdays",
                                "Sundays"].randomElement();
                }
                return offset !== 0 ? rett.toLowerCase() : rett;
            });
            bot.sendMessage(chatID, result);
        }
    })
    .document({
        general: 'Will give you a randomized yes/no answer'
    });

    comm.addCommand('images', ParseNode.noTree(),
    function(parsed, user, chatID){
        let text = '';
        for(let emote in comm.emotes.obj){
            if(comm.emotes.obj.hasOwnProperty(emote)){
                text += `:${emote}: -> ${comm.emotes.obj[emote]}\n`;
            }
        }
        text = text ? text : 'We ran out of emotes ;-;';
        paste(text, 'Images', function(link){
            user.tell(`Images: ${link}`);
        }, 1, true, 'All of me');
    }, Command.group | Command.private)
    .document({
        general: 'Will give you a link that shows you all :emotes:'
    });

    comm.addCommand('version', ParseNode.noTree(),
    function(parsed, user, place){
        bot.sendMessage(place, `${GOVAN_VERSION}${GOVAN_SUBVERSION} (10*)`);
    })
    .document({
        general: 'Will give you the current Govan version'
    });

    comm.addCommand('silence', ParseNode.noTree(),
    function(parsed, user){
        if(!user.subscriptions.get('silence')){
            user.tell(S.silence);
        }
        user.subscriptions.set('silence', !user.subscriptions.get('silence'));
        if(!user.subscriptions.get('silence')){
            user.tell(S.unsilence);
        }
    }, Command.group | Command.private)
    .document({
        general: "Will silence the bot, he won't send you any more PMs"
    });

    comm.addCommand('ultrasilence', ParseNode.noTree(),
    function(parsed, user){
        if(!user.subscriptions.get('ultrasilence')){
            user.tell("I will ignore you, and you won't receive any messages from me anymore");
        }
        user.subscriptions.set('ultrasilence', !user.subscriptions.get('ultrasilence'));
        if(!user.subscriptions.get('ultrasilence')){
            user.subscriptions.set('silence', false);
            user.tell('You will receive my messages again');
        }else{
            user.subscriptions.set('silence', true);
        }
    }, Command.group | Command.private)
    .document({
        general: 'Will make the bot ignore you until you use the command again'
    });

    comm.addCommand('subscriptions', ParseNode.noTree(),
    function(parsed, user){
        let subs = '';
        for(let name in user.subscriptions){
            if(user.subscriptions.hasOwnProperty(name)){
                subs += `${name}: ${user.subscriptions[name] ? 'subscribed' : 'not subscribed'}\n`;
            }
        }
        paste(subs, `Subscriptions for ${user.name}`, function(link){
            user.tell(`Subscriptions: ${link}`);
        }, 5, true);
    }, Command.group | Command.private)
    .document({
        general: 'Will tell you what services you are subscribed to'
    });

    comm.alias('subscriptions', 'subs');

    comm.addCommand('count', ParseNode.noTree(),
    function(parsed, user, place){
        let count = ++bot.botData.persistent.counter;
        bot.sendMessage(place, `${count}${count === 10 ? '' : '(10*)'}`);
    })
    .document({
        general: 'Will add one to the global counter'
    });

    comm.addCommand('clearalias', ParseNode.noTree(),
    function(parsed, user){
        if(user.alias){
            user.alias = '';
            user.tell(`Your alias has been cleared. ` + S.aliasClear);
        }else{
            user.tell(S.noAlias);
        }
    }, Command.group | Command.private)
    .document({
        general: 'Will remove your alias'
    });

    comm.addCommand('fuck', ParseNode.noTree(),
    function(parsed, user, place){
        for(let said of user.lastLines){
            let command = said.split(' ')[0];
            let closest = findClosest(command, comm.groupCommandNames);
            if(closest && comm.groupCommands[closest] !== this){
                comm.parse(closest + said.substring(command.length), user, place, true);
                return;
            }
        }
        user.tell(S.fuck);
    })
    .document({
        general: 'Will look at your recent messages and try to decipher your garbage way of speaking'
    });

    comm.alias('fuck', 'fix');
    comm.alias('fuck', 'fcuk');

    comm.addCommand('secret', ParseNode.noTree(),
    function(){}) //Does nothing
    .document({
        general: 'It does nothing...'
    });

    comm.addCommand('puzzle', ParseNode.noTree(),
    function(parsed, user){
        user.tell("http://sergiovan.me/puzzle.php");
    })
    .document({
        general: 'Will give a link to the puzzle site'
    });

    /* Multiple-words commands */
    /* Help command is a bit special, its tree is defined last */
    let helpCommandNode = new ParseNode(ParseNode.types.string, 'command');
    let help = comm.addCommand('help', ParseNode.noTree(), function(parsed, user, chatID){
        if(parsed[ParseNode.ending] === 0){
            user.tell("http://sergiovan.me/bothelp/");
        }else if(parsed[ParseNode.ending] === 1){
            let cases = comm.generateHelp(parsed['term']);
            if(cases === null){
                user.tell(S.helpConfused);
            }else{
                //Logger.debug(cases);
                let info = parsed.term + '\n';
                if(!cases){
                    user.tell("That command does not exist. How did you get here?");
                    return;
                }
                let command = comm.groupCommands[parsed.term] ||
                              comm.privateCommands[parsed.term] ||
                              comm.guivanCommands[parsed.term];
                let doc = command.doc;
                if(!doc || !Object.keys(doc).length){
                    user.tell(S.helpNoExist);
                    return;
                }
                info += doc.general + '\n\n';
                for(let end = 0; end < cases.length; end++){
                    if(doc[end]){
                        info += doc[end] + '\n';
                        for(let branch of cases[end]){
                            info += '    ';
                            let i = 0;
                            for(let element of branch){
                                if(element !== 'empty:' && element){
                                    if(i % 2 === 1){
                                        info += '[';
                                    }
                                    if(element.charAt(0) === '"'){
                                        info += `${element.split('/').sort().join(' / ')} `;
                                    }else{
                                        info += `${element} `;
                                    }
                                    if(i % 2 === 0 && i !== 0){
                                        info = info.slice(0, -1);
                                        info += '] ';
                                    }
                                    i++;
                                }
                            }
                            info += '\n    or\n'
                        }
                        info = info.slice(0, -3);
                        let example = 'For example: ';
                        let duh = cases[end].length <= 1;
                        for(let element of cases[end].randomElement()){
                            if(element[element.length - 1] !== ':'){
                                switch(element){
                                    case Commander.getName(ParseNode.types.number):
                                        example += randInt(10000) + ' ';
                                        duh = false;
                                        break;
                                    case Commander.getName(ParseNode.types.string):
                                        example += S.helpExampleWords; //TODO Add flavor
                                        example += ' ';
                                        duh = false;
                                        break;
                                    case Commander.getName(ParseNode.types.longString):
                                        example += S.helpExampleSentences;
                                        example += ' ';
                                        duh = false;
                                        break;
                                    case Commander.getName(ParseNode.types.user):
                                        example += users.userArray.randomElement().name.replace(/ /g, '\\ ');
                                        example += ' ';
                                        duh = false;
                                        break;
                                    default:
                                        let possibilities = element.split('/');
                                        if(possibilities.length > 1){
                                            duh = false;
                                        }
                                        example += possibilities.randomElement().replace(/\"/g, '') + ' ';
                                        break;
                                }
                            }
                        }
                        if(duh){
                            example = 'That is: ' + example.slice(13);
                        }
                        example += '\n\n';
                        info += example;
                    }
                }
                let availability = 'Availability: ';
                let groupName = Commander.toGroupCommandName(parsed.term);
                let privateName = Commander.toPrivateCommandName(parsed.term);
                availability += command.isAvailable(Command.group) ? `Group chat (as ${groupName}), ` : '';
                availability += command.isAvailable(Command.private) ? `Private message (as ${privateName}), `: '';
                availability += command.isAvailable(Command.guivan) ? `GUIvan (as ${privateName}), ` : '';
                availability = availability.slice(0, -2);
                if(availability === 'Availability'){
                    availability = 'Availability: None';
                }
                info += availability + '\n';
                info += 'Can I use it? ';
                info += command.test(user) ? 'Yes!' : 'No...';
                info += '\n';
                paste(info, `Help for ${parsed.term}`, (link) => {user.tell(`Help for ${parsed.term}: ${link}`)}, 1);
            }
        }
    });
    help.document({
        general: 'Will help you along with the usage of Govan',
        0: 'To give you a link to the general help site:',
        1: 'To give you specific help with a command:'
    });

    let test = new User(secrets.steam.ownerID);
    let subEndNode = ParseNode.endNode(0);
    let subs = [];
    for(let sub in test.subscriptions){
        if(test.subscriptions.hasOwnProperty(sub)){
            let endNode = subEndNode;
            if(sub === 'silence'){
                endNode = ParseNode.endNode(-1);
            }else if(sub === 'ultrasilence'){
                endNode = ParseNode.endNode(-2);
            }
            subs.push(ParseNode.constant(sub, 'subscription').addChild(endNode));
        }
    }
    comm.addCommand('subscribe', subs,
    function(parsed, user, chatID){
        if(parsed[ParseNode.ending] === 0){
            let finalState = parsed['command'] === '!subscribe' || parsed['command'] === '!sub';
            let current = user.subscriptions.get(parsed['subscription']);
            if(finalState === current){
                //The lengths we go to to not repeat code
                user.tell(`You were already ${parsed['command'].substring(1) + 'd'} to that, silly!`);
            }else{
                user.subscriptions.set(parsed['subscription'], finalState);
                user.tell(`Successfully ${parsed['command'].substring(1) + 'd'}!`);
            }
        }else if(parsed[ParseNode.ending] === -1){
            user.tell('Please use !silence for that');
        }else if(parsed[ParseNode.ending] === -2){
            user.tell('Please use !ultrasilence for that');
        }
    })
    .document({
        general: 'Will change your subscriptions',
        0: 'To subscribe or unsubscribe to a certain service:'
    });

    comm.alias('subscribe', 'unsubscribe');
    comm.alias('subscribe', 'sub');
    comm.alias('unsubscribe', 'unsub');

    comm.addCommand('roll', [
        ParseNode.emptyNode(0),
        ParseNode.string(-1),
        ParseNode.anyNumber('sides').addChild(ParseNode.endNode(1)),
        ParseNode.constant('%').addChild(ParseNode.endNode(2))],
    function(parsed, user, place){
        if(randInt(1000) < 1){
            bot.sendMessage(place, 'The dice broke. Stop throwing it so hard');
            return;
        }
        if(parsed[ParseNode.ending] === 0 || parsed[ParseNode.ending] === -1 || parsed[ParseNode.ending] === 1){
            let dice = parsed['sides'] !== undefined ? parsed['sides'] : 20;
            if(dice < 0){
                bot.sendMessage(place, `Your ${user.rpg.race} mind cannot start to comprehend a negative amount of sides yet`);
                return;
            }else if(dice === 0){
                bot.sendMessage(place, 'The dice rolls 1');
                return;
            }
            let result = randInt(dice) + 1;
            bot.sendMessage(place, `The dice rolls ${result}`);
        }else if(parsed[ParseNode.ending] === 2){
            const ratio = 20 / 100000;
            let result = randInt(100000 + 1);
            let hashes = Math.floor(ratio * result);
            let bar = '[';
            for(let i = 0; i < 20; i++){
                bar += i < hashes ? '#' : '=';
            }
            bar += `] ${(result / 1000).toFixed(3)}%`;
            bot.sendMessage(place, bar);
        }
    })
    .document({
        general: 'Will invoke a random number generator',
        0: 'To roll a d20:',
        1: 'To roll a dice with "sides" sides:',
        2: 'To roll a d10000 and show the results in the form of a progress bar'
    });

    comm.addCommand('id', [
        ParseNode.singleUser(1),
        ParseNode.emptyNode(0)],
    function(parsed, user, chatID){
        if(parsed[ParseNode.ending] === 0){ //Yourself
            bot.sendMessage(chatID, user.id);
        }else if(parsed[ParseNode.ending] === 1){ //Someone else
            bot.sendMessage(chatID, `${parsed.user.id} (${parsed.user.name}` +
                `${parsed.user.alias ? `, alias "${parsed.user.alias}")` : ')'}`);
        }
    })
    .document({
        general: 'Will get you a SteamID',
        0: 'To get your own SteamID:',
        1: "To get some other user's SteamID:"
    });

    comm.addCommand('murder', ParseNode.singleUser(0),
    function(parsed, user){
        if(parsed[ParseNode.ending] === 0){
            if(parsed['user'].subscriptions.get('murder') && user.subscriptions.get('murder')){
                parsed['user'].murder.kill(user);
            }
        }
    }).
    document({
        general: "Will murder someone of your choosing",
        0: "To murder someone:"
    });

    comm.alias('murder', 'stab');
    comm.alias('murder', 'kill');

    comm.addCommand('alias', [
        ParseNode.emptyNode(0),
        ParseNode.longString(1, 'alias')
    ],
    function(parsed, user, place){
        if(parsed[ParseNode.ending] === 0){
            if(user.alias){
                user.tell(`Your alias is ${user.alias}`);
            }else{
                user.tell(S.noAlias);
            }
        }else if(parsed[ParseNode.ending] === 1){
            user.alias = parsed['alias'];
            user.tell(`Your alias is now ${user.alias}`);
        }
    }, Command.group | Command.private)
    .document({
        general: "Will help you with your alias",
        0: "To check what your current alias is:",
        1: "To change your current alias to a different one:"
    });

    comm.addCommand('math', ParseNode.longString(0, 'math'),
    function(parsed, user, place){
        let ret = secrets.puzzle.infixToPolish(parsed.math);
        if(typeof ret === 'string'){
            bot.sendMessage(place, ret);
        }else{
            bot.sendMessage(place, '' + ret + (ret === 10 ? '' : ' (10*)'));
        }
    })
    .document({
        general: "Will try to calculate a result from the given input. Accepts + - / * ^",
        0: "To do math:"
    });

    /* RPG commands */
    comm.addCommand('info', [
        ParseNode.singleUser(1),
        ParseNode.emptyNode(0),
        ParseNode.constant('all').addChild(ParseNode.endNode(2))
    ],
    function(parsed, user, place){
        if(parsed[ParseNode.ending] === 0){ //Yourself
            user.rpg.pasteInfo(place);
        }else if(parsed[ParseNode.ending] === 1){ //Someone else
            parsed['user'].rpg.pasteInfo(place);
        }else if(parsed[ParseNode.ending] === 2){ //Everyone
            users.pasteAll(place);
        }
    }, Command.group | Command.private)
    .document({
        general: "Will give you chatPG info about someone",
        0: "To get info about yourself:",
        1: "To get info about someone in particular:",
        2: "To get ordered info about everyone:"
    });

    /* Grants are non-binding */
    let grantUserNode = ParseNode.userGrant();
    grantUserNode.addChild(ParseNode.emptyNode(0));
    grantUserNode.addChild(ParseNode.longString(-1));
    grantUserNode.addChild(ParseNode.positiveNumber('amount').addChild(ParseNode.endNode(1)));
    grantUserNode.addChild(ParseNode.constant('max').addChild(ParseNode.endNode(2)));
    comm.addCommand('grant', grantUserNode,
    function(parsed, user, place){
        /** @type {User} */
        let other = parsed.user;
        let ending = parsed[ParseNode.ending];
        if(parsed.command !== '!grandom' && user.curses.blind > 0){
            parsed.command = '!grandom';
            comm.groupCommands['!grandom'].funct(parsed, user, place);
            return;
        }
        if(other.id === user.id){
            user.tell(S.sarcasticFunny);
            return;
        }
        let amountGiven;
        /* If granting the same person more than once in a row,
         * granting a new person if your streak is 0,
         * and not doing !grandom.
         * With curse of the blind it increases always */
        let curseIncrease =
            ((other.id === user.rpg.lastPerson) ||
            (user.curses.curseAmount < 1) ||
            (user.curses.blind > 0)) &&
            ((parsed.command !== '!grandom') ||
            (user.curses.blind > 0));
        let free = false, all = false;

        if(ending === 0 || ending === -1){ //Free grant
            let timeLeft = (user.rpg.lastGift + 1000 * 1800 * Math.pow(2, user.curses.timezone)) - (new Date().getTime());
            if(timeLeft > 0){ //Not time yet
                let unit = 'seconds';
                if(randInt(100) < 1){
                    unit = timeUnits.randomElement();
                }
                timeLeft = toTimeUnit(timeLeft, unit);
                user.tell(`${S.grantWait} ${timeLeft} ${unit} to grant free XP again`);
                return;
            }
            user.rpg.lastGift = new Date().getTime() - Math.min(600, user.rpg.hourglasses) * 1000;
            amountGiven = 100 + user.rpg.level * 10;
            amountGiven += Math.ceil(amountGiven * (0.01 * user.rpg.firesInAJar));
            free = true;
        }else if(ending === 1){ //Paid grant
            amountGiven = parsed.amount;
        }else if(ending === 2){ //Max grant
            amountGiven = user.rpg.experience;
            all = true;
        }
        other.rpg.grant(amountGiven, user, free, all);
        if(curseIncrease && free){
            user.curses.curseAmount++;
            user.rpg.lastPerson = other.id;
        }else{
            user.curses.curseAmount--;
            user.curses.curseAmount = Math.max(0, user.curses.curseAmount);
        }
        user.curses.curse();
        if(free && parsed.command === '!grandom' && user.curses.blind === 0){
            bot.botData.grantStreak++;
            if(randInt(100) < bot.botData.grantStreak){
                bot.botData.grantStreak = 0;
                if(randInt(100) < 50 || user.rpg.hourglasses > 599){
                    user.rpg.firesInAJar++;
                    user.tell(S.getFireInJar);
                }else{
                    user.rpg.hourglasses++;
                    user.tell(S.getMagicHourglass);
                }
            }
        }
    })
    .document({
        general: "Will grant XP to someone",
        0: "To grant free XP to someone:",
        1: "To grant a set amount of XP to someone from your own XP:",
        2: "To grant all your XP to someone:"
    });

    comm.addCommand('grandom', [
            ParseNode.emptyNode(0),
            ParseNode.longString(-1),
            ParseNode.positiveNumber('amount').addChild(ParseNode.endNode(1)),
            ParseNode.constant('max').addChild(ParseNode.endNode(2))
    ],
    function(parsed, user, place){
        let here = Object.keys(bot.chatRooms[place]);
        if(here.length < 3){
            user.tell('Not enough users present, use !grant instead');
            return;
        }
        here.splice(here.indexOf(user.id, 1));
        here = here.filter(Users.active());
        if(here.length < 1){
            user.tell('Not enough eligible users');
            return;
        }
        parsed.user = users[here.randomElement()];
        comm.groupCommands['!grant'].funct(parsed, user, place);
    })
    .document({
        general: "Will grant XP to someone at random that is currently in the chat",
        0: "To grant free XP to someone at random:",
        1: "To grant a set amount of XP from your own XP to someone at random:",
        2: "To grant all your XP to someone at random"
    });

    comm.addCommand('exorcise', ParseNode.noTree(),
    function(parsed, user, place){
        let c = user.curses;
        let count = c.amnesia + c.blind + c.derpy + c.govan + c.illiteracy + c.timezone;
        if(count === 0){
            user.tell("You've been a good boy, no curses on you");
        }else{
            let cost = count * 30;
            if(user.rpg.payLevels(cost)){ //TODO This is not OK
                user.curses.clear();
                user.tell("You're free to go");
            }else if(user.curses.hopeless()){
                user.curses.clear();
                user.rpg.payXP(user.rpg.experience, true);
                user.rpg.lastGift = 0;
                user.rpg.lastPerson = '';
                user.tell("You were completely hopeless. You are reset to level 1 with no XP and no curses. Behave " +
                    "now, will you?");
            }else{
                user.tell("You don't have enough levels for that. Enjoy your curses");
            }
        }
    })
    .document({
        general: "Will remove all your curses for the small cost of 30 levels per curse, or all your dignity"
    });

    comm.addCommand('buy', [
        ParseNode.constant('achievement', 'product').addChild(ParseNode.endNode(0)),
        ParseNode.constant('welcome', 'product').addChild(ParseNode.longString(1)),
        ParseNode.constant('farewell', 'product').addChild(ParseNode.longString(2)),
        ParseNode.constant('disconnect', 'product').addChild(ParseNode.longString(3)),
        ParseNode.constant('bail', 'product').addChild(ParseNode.longString(4)),
        ParseNode.constant('ban', 'product').addChild(ParseNode.longString(5)),
        ParseNode.constant('lore', 'product').addChild(ParseNode.longString(6)),
        ParseNode.constant('prefix', 'product').addChild(ParseNode.string(7)),
        ParseNode.constant('race', 'product').addChild(ParseNode.string(8)),
        ParseNode.constant('class', 'product').addChild(ParseNode.string(9))
    ],
    function(parsed, user, place){
        const prices = [100, 50, 25, 25, 25, 1, 10, 50, 10, 10];
        let product = parsed[ParseNode.ending];
        let success = user.rpg.payLevels(prices[product]);
        if(parsed.longString){
            parsed.longString = ExternalData.censor(parsed.longString);
        }
        if(parsed.string){
            parsed.string = ExternalData.censor(parsed.string);
        }
        if(success){
            switch(product){
                case 0: //Achievement
                    user.tell('Achievement get! It might take a while to update, be patient');
                    users[secrets.steam.ownerID].tell(`${user.name} (${user.id}) bought the achievement`);
                    break;
                case 1: //Welcome
                    user.tell(`Your welcome message is now "${parsed.longString}"`);
                    user.rpg.bought.welcome = parsed.longString;
                    break;
                case 2: //Farewell
                    user.tell(`Your farewell message is now "${parsed.longString}"`);
                    user.rpg.bought.farewell = parsed.longString;
                    break;
                case 3: //Disconnect
                    user.tell(`Your disconnect message is now "${parsed.longString}"`);
                    user.rpg.bought.disconnect = parsed.longString;
                    break;
                case 4: //Bail
                    user.tell(`Your kick message is now "${parsed.longString}"`);
                    user.rpg.bought.kick = parsed.longString;
                    break;
                case 5: //Ban
                    user.tell(`Your ban message is now "${parsed.longString}"`);
                    user.rpg.bought.ban = parsed.longString;
                    break;
                case 6: //Lore
                    user.tell(`Your lore is now "${parsed.longString}"`);
                    user.rpg.bought.lore = parsed.longString;
                    break;
                case 7: //Prefix
                    user.rpg.prefix = parsed.string;
                    user.tell(`Your prefix is now "${parsed.string}": ${user.rpgname}`);
                    break;
                case 8: //Race
                    user.rpg.race = parsed.string;
                    user.tell(`Your race is now "${parsed.string}"`);
                    break;
                case 9: //Class
                    user.rpg.class = parsed.string;
                    user.tell(`Your class is now "${parsed.string}"`);
                    break;
            }
        }else{
            user.tell(S.buyNoLevels);
        }
    }, Command.group | Command.private)
    .document({
        general: "Will let you buy something, provided you have the levels for it",
        0: "To buy the achievement for 100 levels:",
        1: "To buy a welcome message for when you come into the chat for 50 levels:",
        2: "To buy a farewell message for when you leave the chat for 25 levels:",
        3: "To buy a disconnect message for when you disconnect from steam for 25 levels:",
        4: "To buy a bail message for when you are kicked from the chat for 25 levels:",
        5: "To buy a ban message for when you are banned from the chat (;-;) for 1 level:",
        6: "To buy some lore to show on your info page for 10 levels:",
        7: "To buy a prefix for your name for 50 levels:",
        8: "To buy a race for yourself for 10 levels",
        9: "To buy a class for yourself for 10 levels"
    });

    //endregion

    //region Private/GUIvan commands

    /* Trusted user commands */
    comm.addCommand('upload', ParseNode.noTree(),
    function(parsed, user){
        upload(function(success){
            if(success){
                Logger.info('All files uploaded successfully');
            }else{
                Logger.error('Not all files were uploaded');
            }
        });
    }, Command.guivan | Command.private, Constraint.isTrusted())
    .document({
        general: "Will upload the logs"
    });

    comm.addCommand('say', ParseNode.longString('talky'),
    function(parsed, user, place){
        let string = ExternalData.censor(parsed['longString']);
        if(user.id !== secrets.steam.ownerID){
            string = `[${user.name}] ` + string;
            if(steamLinkRegex.test(string)){
                user.tell("That message appears to contain a link. I'd rather not send it");
                return;
            }
        }
        bot.sendMessage(bot.botData.chat, string);
    }, Command.guivan | Command.private, Constraint.isTrusted())
    .document({
        general: "Will make Govan say something in chat",
        0: "To make Govan speak words of wisdom and glory:"
    });

    comm.addCommand('votekick', ParseNode.singleUser(0),
    function(parsed, user, place){
        if(bot.botData.voting){
            let timeleft = bot.botData.timeout._idleTimeout - (new Date().getTime() - bot.botData.timeout.startMillis);
            let unit = 'seconds';
            if(randInt(100) < 1){
                unit = timeUnits.randomElement();
            }
            user.tell(`${S.voteInProgress}Please wait ${toTimeUnit(timeleft, unit)} ${unit} to ` +
                      `start another one`);
        }else{
            let kicking = parsed.user;
            let active = Object.keys(bot.chatRooms[place]).filter(Users.currentActive());
            if(user.id === kicking.id){
                user.tell(S.sarcasticFunny);
            }else if(active.length < 3){
                user.tell("There's not enough active users for that right now");
            }else if(!bot.chatRooms[place][kicking.id]){
                user.tell("That user is not even in the chat right now...");
            }else if(!active.includes(kicking.id)){
                user.tell("That user is not even active right now...");
            }else if(!kicking.subscriptions.get("kick")){
                bot.sendMessage(place, "That user does not like democracy");
            }else{
                let needed = Math.max(3, Math.ceil(active.length * 2 / 3));
                for(let u of Object.keys(bot.chatRooms[place])){
                    bot.botData.voters[u] = false;
                }
                bot.sendMessage(place, `Vote started to kick ${kicking.name}. Votes needed to proceed: ${needed}.` +
                                ` Vote with 'Vote' or '!vote'`);
                bot.botData.startVote(needed, function(kick){
                    if(kick){
                        bot.sendMessage(place, "Vote over. " + S.voteSuccess);
                        bot.kick(place, kicking.id);
                    }else{
                        bot.sendMessage(place, `Vote over. ${kicking.rpgname}${S.voteFailure}`);
                    }
                });
            }
        }
    }, Command.group, Constraint.isTrusted())
    .document({
        general: "Vote to kick someone",
        0: "To start a vote to kick someone:"
    });

    let voteNodes = [];
    voteNodes.push(ParseNode.emptyNode(0));
    for(let yes of ['yes', 'y', 'da', 'ja']){
        voteNodes.push(ParseNode.constant(yes, 'vote').addChild(ParseNode.endNode(1)));
    }
    for(let no of ['no', 'n', 'nyet']){
        voteNodes.push(ParseNode.constant(no, 'vote').addChild(ParseNode.endNode(2)));
    }
    for(let we of ['whatever', 'we', 'w', 'meh']){
        voteNodes.push(ParseNode.constant(we, 'vote').addChild(ParseNode.endNode(3)));
    }
    comm.addCommand('vote', voteNodes, function(parsed, user, place){
        let ending = parsed[ParseNode.ending];
        if(ending === 0 || !user.isTrusted() || !bot.botData.voting){ //Give information about current vote
            if(!bot.botData.voting){
                user.tell("There is no vote going on at the moment");
            }else{
                let timeleft = bot.botData.timeout._idleTimeout - (new Date().getTime() - bot.botData.timeout.startMillis);
                let unit = 'seconds';
                let enough = bot.botData.votesGotten >= bot.botData.votesNeeded;
                let diff = bot.botData.yesVotes - bot.botData.noVotes;
                user.tell(`Current vote will still go on for ${toTimeUnit(timeleft, unit)} ${unit}.\n` +
                         `With ${bot.botData.votesGotten} votes casted out of ${bot.botData.votesNeeded} needed, ` +
                         `${enough ? '' : 'the vote may not complete. However,'} if the voting were to end right now,` +
                         ` ${diff > 0 ? `"Yes"` : `"No"`} would win by ${Math.abs(diff)} votes.`);
            }
        }else if(ending === 1){
            bot.botData.vote(user, BotData.votes.yes);
        }else if(ending === 2){
            bot.botData.vote(user, BotData.votes.no);
        }else if(ending === 3){
            bot.botData.vote(user, BotData.votes.whatever);
        }
    }, Command.group | Command.private | Command.guivan)
        .document({
            general: "To cast a vote or get information about the current vote. If you're not trusted you'll only be" +
            " able to get information on the current ongoing vote",
            0: "To have Govan tell you the status of the vote:",
            1: "To vote yes:",
            2: "To vote no:",
            3: "To vote blank:"
        });

    comm.addCommand('trust', [ParseNode.singleUser(1), ParseNode.emptyNode(0)],
    function(parsed, user, place){
        if(parsed[ParseNode.ending] === 0){
            if(user.untrusted){
                bot.sendMessage(place, S.distrusted);
            }else if(!user.isTrusted()){
                bot.sendMessage(place, 'You are not a trusted user');
            }else{
                bot.sendMessage(place, S.trusted)
            }
        }else if(parsed[ParseNode.ending] === 1){
            if(user.isTrusted()){
                if(parsed['user'].untrusted && user.isSuperUser()){
                    parsed['user'].untrusted = false;
                    parsed['user'].tell('You are no longer untrusted');
                    user.tell(`${parsed['user'].name} can now be trusted again`);
                }else if(parsed['user'].untrusted){
                    user.tell('That user cannot be trusted');
                }else if(parsed['user'].trusted){
                    user.tell('I already trusted that user');
                }else{
                    parsed['user'].trusted = true;
                    parsed['user'].tell('You are now trusted');
                    user.tell(`${parsed['user'].name} is now trusted`);
                }
            }else{
                user.tell("I don't trust you with that");
            }
        }
    }, Command.group | Command.private)
    .document({
        general: "Will give you information about someone's trust",
        0: "To see your own trusted status:",
        1: "To trust another user"
    });

    comm.addCommand('account', [
        ParseNode.constant('register', 'method').addChild(ParseNode.endNode(0)),
        ParseNode.constant('password', 'method').addChild(ParseNode.endNode(1)),
        ParseNode.constant('recreate', 'method').addChild(ParseNode.endNode(2))
    ], function(parsed, user, place){
        let consoleUser = io.users.obj.find((u) => u.userID === user.id);
        if(consoleUser){
            if(parsed[ParseNode.ending] === 1){
                user.secretTell(`Your password is ${consoleUser.pass}`);
            }else if(parsed[ParseNode.ending] === 2){
                let password = Server.genPassword();
                consoleUser.pass = password;
                io.users.save();
                user.tell(`Your new password is ${password}`);
            }else if(parsed[ParseNode.ending] === 0){
                user.tell('You already have an account, silly');
            }
        }else{
            if(parsed[ParseNode.ending] === 0){
                let password = Server.genPassword();
                io.users.obj.push(new ConsoleUser(user.name, password, user.id));
                io.users.save();
                user.secretTell(`This is your password: ${password}\nDon't tell anyone!`)
            }else{
                user.tell(`Register first with "Account register"`);
            }
        }
    }, Command.private, Constraint.isTrusted())
    .document({
        general: "Will let you control your GUIvan account",
        0: "To register a GUIvan account",
        1: "To have Govan tell you your password again",
        2: "To change your password"
    });

    comm.addCommand('alert', ParseNode.noTree(),
    function(parsed, user){
        for(let superuser of users.userArray.filter((u) => u.isSuperUser())){
            superuser.tell(`${user.name} alerted you`);
        }
    }, Command.group, Constraint.isTrusted())
    .document({
        general: "Will alert moderators and admins. Only for emergencies (e.g. someone spamming in chat)"
    });

    comm.addCommand('ghost', ParseNode.noTree(),
    function(parsed, user){
        if(bot.botData.ghostTimer > now()){
            user.tell(S.ghostTooSoon + '. Rejoin the chat ' +
                'with this link: steam://friends/joinchat/' + bot.botData.chat);
        }else{
            bot.botData.ghostTimer = now() + 5 * 60 * 1000;
            user.tell('Everyone has been notified. Rejoin yourself with this link: ' +
                'steam://friends/joinchat/' + bot.botData.chat);
            let chatRoom = bot.chatRooms[bot.botData.chat];
            for(let userID in chatRoom){
                if(chatRoom.hasOwnProperty(userID) && users[userID] && users[userID].subscriptions.ghost &&
                    users[userID] !== user){
                    users[userID].tell(S.ghostNotified + '. Rejoin with this link: ' +
                        'steam://friends/joinchat/' + bot.botData.chat);
                }
            }
            bot.leaveChat(bot.botData.chat);
            bot.joinChat(bot.botData.chat);
        }
    }, Command.private | Command.guivan, Constraint.isTrusted())
    .document({
        general: "Will inform Govan that the chat has ghosted out, and will make Govan inform all with the Ghost " +
        "subscription"
    });

    /* Superuser commands */
    comm.addCommand('untrust', ParseNode.singleUser(0),
    function(parsed, user){
        if(parsed['user'].trusted){
            io.users.obj = io.users.obj.filter((u) => u.userID !== parsed['user'].id);
            io.users.save();
            parsed['user'].trusted = false;
            parsed['user'].tell('You are no longer trusted');
            user.tell(`${parsed['user'].name} is no longer trusted`);
        }else if(!parsed['user'].untrusted){
            parsed['user'].untrusted = true;
            parsed['user'].tell('You are now distrusted');
            user.tell(`${parsed['user'].name} is now distrusted`);
        }else{
            user.tell(`That user was already distrusted`);
        }
    }, Command.guivan | Command.private, Constraint.isSuperUser())
    .document({
        general: "Will remove trust or untrust someone",
        0: "To remove trust or, if the user has no trust, make them untrusted:"
    });

    comm.addCommand('die', ParseNode.noTree(),
    function(){
        exit();
    }, Command.guivan | Command.private, Constraint.isSuperUser())
    .document({
        general: "Will shut down Govan entirely"
    });

    comm.addCommand('reload', ParseNode.noTree(),
    function(){
        try{
            let SS = {};
            delete require.cache[require.resolve('./strings.js')];
            SS = require('./strings.js');
            S = SS;
            Logger.info("Strings reloaded");
        }catch(anything){
            Logger.error(anything);
            Logger.info("Old strings kepts");
        }
    }, Command.guivan | Command.private, Constraint.isSuperUser())
    .document({
        general: "Will reload all strings that are stored in strings.js"
    });

    comm.addCommand('cancelVote', ParseNode.noTree(),
    function(parsed, user){
        if(!bot.botData.voting){
            user.tell("No vote going on at the moment");
        }else{
            bot.botData.terminateVote();
            user.tell("Vote forcefully ended");
        }
    }, Command.guivan | Command.private, Constraint.isSuperUser())
    .document({
        general: "Will end a voting forcibly, without having the result affect anything"
    });

    const modes = Object.keys(BotData.modes);
    let modeNodes = [];
    let modeHelp = {
        general: "Will change Govan's mode"
    };
    for(let key of modes){
        modeNodes.push(ParseNode.constant(key).addChild(ParseNode.endNode(BotData.modes[key])));
        modeHelp[BotData.modes[key]] = `To set Govan to mode "${key}"`;
    }
    modeNodes.push(ParseNode.string(-1, 'mode'));
    comm.addCommand('mode', modeNodes,
    function(parsed, user, place){
        if(parsed[ParseNode.ending] === -1){
            user.tell(`Mode "${parsed.mode}" not supported`);
        }else{
            bot.botData.mode = BotData.modes[parsed.constant];
            user.tell(`Mode changed to "${parsed.constant}"`);
        }
    }, Command.guivan | Command.private, Constraint.isSuperUser())
    .document(modeHelp);

    comm.addCommand('add', ParseNode.longString(0, 'emote'),
    function(parsed, user){
        if(parsed.emote.indexOf('|') === -1){
            user.tell('No separator found. Format is {emote name} | {emote link}');
            return;
        }else if(parsed.emote.indexOf('|') !== parsed.emote.lastIndexOf('|')){
            user.tell('Too many separators found. Format is {emote name} | {emote link}');
            return;
        }
        let emote = parsed.emote.split('|');
        comm.addEmote(...emote); //Amazing
        user.tell(`Emote added, use with :${emote[0].trim()}:`);
    }, Command.guivan | Command.private, Constraint.isSuperUser())
    .document({
        general: "Will add an :emote: to Govan's database",
        0: "To add an :emote: (name | link):"
    });

    comm.addCommand('joke', ParseNode.longString(0, 'joke'),
    function(parsed, user){
        ExternalData.addJoke(parsed.joke);
        user.tell('Joke added!');
    }, Command.guivan | Command.private, Constraint.isSuperUser())
    .document({
        general: "Will add a joke to Govan",
        0: "To add a joke:"
    });

    comm.addCommand('leave', ParseNode.noTree(),
    function(){
        bot.sendMessage(bot.botData.chat, "Off I go!");
        bot.leaveChat();
    }, Command.guivan | Command.private, Constraint.isSuperUser())
    .document({
        general: "Will make Govan leave his main chat"
    });

    comm.addCommand('rejoin', ParseNode.noTree(),
    function(){
        bot.leaveChat();
        bot.joinChat(bot.botData.chat);
    }, Command.guivan | Command.private, Constraint.isSuperUser())
    .document({
        general: "Will make Govan leave his main chat and then rejoin again immediately"
    });

    comm.addCommand('join', ParseNode.noTree(),
    function(){
        bot.joinChat(bot.botData.chat);
    }, Command.guivan | Command.private, Constraint.isSuperUser())
    .document({
        general: "Will make Govan join his main chat"
    });

    /* Admin commands */
    function setPower(power, set){
        return function(parsed, user){
            if(parsed['user'][power] === !set){
                parsed['user'][power] = set;
                parsed['user'].tell(`You are ${set ? 'now' : 'no longer'} a bot ${power}`);
                user.tell(`${parsed['user'].name} is ${set ? 'now' : 'no longer'} a bot ${power}`);
            }else{
                user.tell(`${parsed['user'].name} ${set ? 'already is' : "already isn't"} a bot ${power}`);
            }
        }
    }
    comm.addCommand('admin',  ParseNode.anySingleUser(0),
        setPower('admin', true), Command.guivan | Command.private, Constraint.isAdmin())
    .document({
        general: "Will make someone a bot admin",
        0: "To make someone a bot admin:"
    });

    comm.addCommand('deadmin', ParseNode.anySingleUser(0),
        setPower('admin', false), Command.guivan | Command.private, Constraint.isAdmin())
        .document({
            general: "Will deadmin someone",
            0: "To remove admin status from someone:"
        });

    comm.addCommand('op', ParseNode.anySingleUser(0),
        setPower('superuser', true), Command.guivan | Command.private, Constraint.isAdmin())
        .document({
            general: "Will make someone a bot superuser",
            0: "To make someone a bot superuser:"
        });

    comm.addCommand('deop', ParseNode.anySingleUser(0),
        setPower('superuser', false), Command.guivan | Command.private, Constraint.isAdmin())
        .document({
            general: "Will make someone lose superuser status",
            0: "To remove someone from a position of power and seize back the means of production:"
        });

    comm.addCommand('ban', ParseNode.anySingleUser(0),
    function(parsed, user){
        if(!parsed['user'].banned){
            parsed['user'].banned = true;
            parsed['user'].tell('You have been banned from using Govan');
            user.tell(`${parsed['user'].name} is now banned from using Govan`);
        }else{
            user.tell(`${parsed['user'].name} user is already banned`);
        }
    }, Command.guivan | Command.private, Constraint.isAdmin())
    .document({
        general: "Will bot ban someone",
        0: "To bot ban someone:"
    });

    comm.addCommand('unban', ParseNode.anySingleUser(0),
    function(parsed, user){
        if(parsed['user'].banned){
            parsed['user'].banned = false;
            parsed['user'].tell('You have been unbanned from using Govan');
            user.tell(`${parsed['user'].name} is now unbanned from using Govan`);
        }else{
            user.tell(`${parsed['user'].name} user is not banned`);
        }
    }, Command.guivan | Command.private, [Constraint.isAdmin()])
    .document({
        general: "Will remove someone's botban",
        0: "To unban someone"
    });

    comm.addCommand('purge', ParseNode.anySingleUser(0),
    function(parsed, user){
        delete users._users.obj[parsed.user.id];
        users._users.save();
        user.tell(`Removed ${parsed.user.name || parsed.user.id || 'user'} from database`);
    }, Command.guivan | Command.private, Constraint.isAdmin())
    .document({
        general: "Will remove someone from the bot's database",
        0: "To purge someone:"
    });

    comm.addCommand('superdie', ParseNode.noTree(),
    function(){
        abort();
    }, Command.private | Command.guivan, Constraint.isAdmin())
    .document({
        general: "Will murder Govan without any safety measures"
    });

    comm.addCommand('ip', ParseNode.noTree(),
    function(parsed, user){
        let getIP = extIP();
        getIP(function(err, ip){
            if(err){
                user.secretTell('Error while getting the IP');
                Logger.error(err);
            }else{
                user.secretTell(`IP: ${ip}`);
            }
        });
    }, Command.private, Constraint.isAdmin())
    .document({
        general: "Will give Govan's current IP address"
    });

    //endregion

    //region Help
    helpCommandNode.addChild(ParseNode.emptyNode(0));
    let specificHelpNodeEnd = ParseNode.endNode(1);
    let allCommands = new Set(comm.groupCommandNames.concat(comm.privateCommandNames).concat(comm.guivanCommandNames));
    for(let command of allCommands){
        helpCommandNode.addChild(ParseNode.constant(command, 'term').addChild(specificHelpNodeEnd))
    }
    help.startNode = helpCommandNode;
    //endregion

    //endregion

    //region Finds

    comm.addFind('joke', function(joke, user, place){
        bot.sendMessage(place, ExternalData.joke);
    }, true);

    comm.addFind(['log', 'logs'], function(log, user, place){
        user.tell(`Here you go: ${secrets.sergio.link}`);
    }, true);

    comm.addFind('fuck off', function(fuck, user, place){
        bot.sendMessage(place, 'No, you fuck off');
    }, true);

    comm.addFind('i love you', function(love, user, place){
        bot.sendMessage(place, ':D');
    }, true);

    comm.addFind('vroom vroom', function(vroom, user, place){
        if(randInt(5) < 1 || user.attention){
            bot.sendMessage(place, 'Vroom vroom');
            user.attention = false;
        }
    });

    comm.addFind(['thanks', 'thank you'], function(thanks, user, place){
        bot.sendMessage(place, "You're welcome");
    }, true);

    comm.addFind('nothing', function(nothing, user, place){
        bot.sendMessage(place, 'Alright');
    }, true);

    comm.addFind(secrets.puzzle.sk, function(key, user, place){
        users[secrets.steam.ownerID].tell(`${user.name} wins`);
    });

    //endregion

    new cron.CronJob('0 * * * * *', function(){
        if (!stclient.loggedOn && stclient.connected) {
            stclient.login();
        }
    }, null, true);

    new cron.CronJob('00 00 12 18 04 *', () => say(secrets.mainChat, "Happy birthday to me!"), null, true);

    new cron.CronJob('00 00 00 * * *', function(){
        for(let user of users.userArray){
            if(user.curses.amnesia > 0){
                user.rpg.payLevels(user.curses.amnesia, true);
            }
        }
    }, null, true);

    stclient.connect();
    Logger.info(`Started!`);
    bot.botData.persistent.runs++;
    bot.botData.persistent.save();
}

/**
 * For peaceful exits, when nothing went wrong
 * */
function exit(){
    try{
        bot.sendMessage(bot.botData.chat, S.slain);
    }catch(anything){ /* No worries */ }
    users._users.save(function(){
        upload(function(){
            bot.leaveChat(bot.botData.chat);
            stclient.disconnect();
            Logger.status('Disconnected');
            io.io.close();
            process.exit(0); //Find a better way of doing this, ffs
        });
    });
}

/**
 * For extreme emergencies
 */
function abort(){
    try{
        bot.sendMessage(bot.botData.chat, 'AVENGE ME');
        Logger.status('Forcibly disconnected');
    }catch(anything){
        console.error('RRRRRRRRRRRR');
    }finally{
        process.exit(1);
    }
}

/** Used by the magic that is logging onto steam */
function makeSha(bytes) {
    var hash = crypto.createHash('sha1');
    hash.update(bytes);
    return hash.digest();
}

/** Used to make target adquire all properties of model */
function fuse(target, model){
    for(let prop in model){
        if(model.hasOwnProperty(prop)){
            target[prop] = model[prop];
        }
    }
}

/** Used to make 2 objects have the same keys */
function onlyThis(target, model){
    for(let key in target){
        if(target.hasOwnProperty(key)){
            if(model.hasOwnProperty(key)){
                if(typeof model[key] !== typeof target[key]){
                    target[key] = clone(model[key]);
                }
            }else{
                delete target[key];
            }
        }
    }
    for(let key in model){
        if(model.hasOwnProperty(key)){
            if(!target.hasOwnProperty(key)){
                target[key] = clone(model[key]);
            }
        }
    }
}

/** Creates a file if it doesn't exist, else does nothing */
function createFile(path){
    try{
        fs.accessSync(path, fs.R_OK | fs.W_OK);
    }catch(err){ //File did not exist
        Logger.debug('Created file in ' + path);
        fs.writeFileSync(path, '');
    }
}

/**
 * Callback for readOrCreate()
 * @callback readOrCreateCallback
 * @param {String} file The contents of the file
 * @returns {*}
 */

/**
 * Reads a file or creates it if it doesn't exist. Returns whatever cb returns
 * @param {String} path Path to file
 * @param {readOrCreateCallback} cb A callback function
 * @returns {*}
 */
function readOrCreate(path, cb){
    createFile(path);
    let content = "";
    try{
        content = fs.readFileSync(path).toString();
    }catch(err){
        Logger.error(err);
    }
    return cb(content);
}

/**
 * Returns a random element from the given array
 * @param {(Array|String)} array The array to get an element from
 * @returns {*} A random element from the array, or null if no element was passed
 */

/**
 * Damerau-Levenshtein distance between 2 words. From stackoverflow
 * @param  {String} a First word
 * @param  {String} b Second word
 * @return {Number}   The Damerau-Levenshtein distance
 */
function wordDistance(a, b){
    let i, j, cost;
    let d = [];

    if(a.length === 0){
        return b.length;
    }

    if(b.length === 0){
        return a.length;
    }

    for(i = 0; i <= a.length; i++){
        d[i] = [];
        d[i][0] = i;
    }

    for(j = 0; j <= b.length; j++){
        d[0][j] = j;
    }

    for(i = 1; i <= a.length; i++){
        for(j = 1; j <= b.length; j++){
            if(a.charAt(i - 1) === b.charAt(j - 1)){
                cost = 0;
            }else{
                cost = 1;
            }
            d[i][j] = Math.min(d[i-1][j] + 1, d[i][j-1] + 1, d[i-1][j-1] + cost);
            if(i > 1 && j > 1 &&  a.charAt(i-1) === b.charAt(j-2) && a.charAt(i-2) === b.charAt(j-1)){
                d[i][j] = Math.min(d[i][j],d[i-2][j-2] + cost);
            }
        }
    }
    return d[a.length][b.length];
}

/**
 * @callback closestError
 * @param {String} word The word that was being searched for
 * @param {String} found The word found
 * @param {Number} distance The distance between the words
 * @returns {Boolean} If the error is acceptable
 */
function closestError(word, found, distance){
    if(distance === undefined){
        distance = wordDistance(word, found);
    }
    return (Math.max(found.length - 1, 4))/2 > distance;
}

/**
 * Finds the closest element in array to word
 * @param {String} word
 * @param {String[]} array An array of strings
 * @param {closestError} errorCheck How to determine if the found string is close enough
 */
function findClosest(word, array, errorCheck = closestError){
    let closest = null, closeness = Number.POSITIVE_INFINITY;
    for(let element of array){
        let dist = wordDistance(word, element);
        if(dist === 0){
            return element; //Word found in the array as is
        }
        if(dist < closeness){
            closeness = dist;
            closest = element;
        }
    }
    if(errorCheck instanceof Function){
        return errorCheck(word, closest, closeness) ? closest : null;
    }else{
        throw new Error('errorCheck is not a function: ' + errorCheck);
    }
}

/**
 * Finds the text in a string encapsulated between 2 symbols, with nesting
 * @param {String} str The string
 * @param {Number} start Starting position
 * @param {String} openingSymbol Opening symbol
 * @param {String} closingSymbol Closing symbol
 * @returns {String} Substring between symbols, without the symbols
 */
function findEnclosing(str, start = 0, openingSymbol = '{', closingSymbol = '}'){
    let count = 0;
    let ssstart, ssend;
    for(let i = start; i < str.length; i++){
        if(str.charAt(i) === openingSymbol && !(openingSymbol === closingSymbol && ssstart)){
            if(!count){
                ssstart = i;
            }
            count++;
        }else if(str.charAt(i) === closingSymbol){
            count--;
            if(!count){
                ssend = i;
                return str.substring(ssstart + 1, ssend);
            }
            if(count < 0){
                count++;
            }
        }
    }
    return null;
}

/**
 * Returns true if obj is a number that's not infinite
 * @param {*} obj The object to test for
 * @returns {boolean} If it is a number or not
 */
function isNumber(obj){
    if(typeof obj !== 'number' && typeof obj !== 'string'){
        return false;
    }
    let parsed = Number.parseFloat(obj);
    return !Number.isNaN(parsed) && Number.isFinite(parsed);
}

/**
 * Return true if the string given is a valid SteamID64
 * @param str The string to test
 * @returns {boolean} If it's a SteamID64
 */
function isUserID(str){
    return isNumber(str) && str.length === secrets.steam.ownerID.length;
}

/**
 * Returns true if the string given is a valid GroupID64
 * @param {String} str The Group ID to test for
 * @returns {boolean}
 */
function isGroupID(str){
    return isNumber(str) && str.length === secrets.steam.mainChat.length;
}

/**
 * Gets a property of an object from a string.
 * Property name may not contain '.'
 * @param {*} obj The object to get the property from
 * @param {string} path The path
 * @param {boolean} safe Won't throw if it encounters undefined
 * @returns {*} The property
 */
function getPropertyByPath(obj, path, safe = false){
    if(path === null){
        return obj;
    }
    let pathArr = path.split('.');
    let curLayer = obj;
    for(let elem of pathArr){
        if(curLayer.hasOwnProperty(elem)){
            curLayer = curLayer[elem];
        }else{
            if(safe) {
                return undefined;
            }else{
                throw new Error(`${util.inspect(obj)} has nothing with path '${path}'` +
                                `, failed at '${elem}' in ${util.inspect(curLayer)}`);
            }
        }
    }
    return curLayer;
}

function setPropertyByPath(obj, path, value, safe = false){
    let pathArr = path.split('.');
    let curLayer = obj;
    for(let elem of pathArr){
        if(curLayer.hasOwnProperty(elem)){
            if(pathArr[pathArr.length - 1] === elem){
                curLayer[elem] = value;
            }else{
                curLayer = curLayer[elem];
            }
        }else{
            if(safe) {
                return undefined;
            }else{
                throw new Error(`${util.inspect(obj)} has nothing with path '${path}'` +
                    `, failed at '${elem}' in ${util.inspect(curLayer)}`);
            }
        }
    }
}

/**
 * Returns an integer between 0 and start if end is not defined, or between start and end.
 * End of the range never included, start always included
 * @param {Number} start
 * @param {Number} end
 * @returns {Number}
 */
function randInt(start, end = null){
    if(!end){
        return Math.floor(Math.random() * start);
    }else{
        return Math.floor(Math.random() * (end - start)) + start;
    }
}

function tenize(number, level){
    let digits = Math.max(0, Math.round(Math.log(number) / Math.log(10)));
    let rett = Math.pow(10, digits - level);
    return ((rett < 10) ? 0 : rett);
}

const timeUnits = ['0-time units', 'milliseconds', 'seconds', 'minutes', 'hours', 'days', 'weeks', 'years',
    'decades', 'centuries', 'eternities'];
function toTimeUnit(millis, unit = 'seconds'){
    if(typeof unit === 'string'){
        unit = timeUnits.indexOf(unit);
    }
    if(unit < 0 || unit >= timeUnits.length){
        throw new Error('Unsuported unit: ' + unit);
    }
    let ratio = 1;
    switch(unit){
        case 10: //Eternities
            ratio = Infinity;
            break;
        case 9: //Centuries
            ratio *= 10;
        case 8: //Decades
            ratio  *= 10;
        case 7: //Years
            ratio *= 365;
        case 6: //Weeks
            ratio *= 7;
        case 5: //Days
            ratio *= 24;
        case 4: //Hours
            ratio *= 60;
        case 3: //Minutes
            ratio *= 60;
        case 2: //Seconds
            ratio *= 1000;
        case 1: //Milliseconds
            break;
        case 0: //0-time units
            ratio = 0;
            break;
    }
    return millis / ratio;
}

function tagRemoveIndentation(strings, ...values){
    let output = '';
    for(let i = 0; i < values.length; i++){
        output += strings[i] + values[i];
    }
    output += strings[values.length];
    output = output.replace(/^\s+/gm, '');
    return output;
}

function now(){
    return new Date().getTime();
}

function later(minutes){
    return new Date().getTime() + (1000 * 60 * minutes);
}

function getYoutubeID(string){
    const regex = /^.*(youtu\.be\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\? ]*).*/;
    let match = string.match(regex);
    if(match && match[2].length === 11){
        return match[2];
    }else{
        return null;
    }
}

function getTwitchID(string){
    const regex = /(?:(?:www\.|\/\/)twitch\.tv\/)(.*?)(?= |$)/;
    let match = string.match(regex);
    if(match){
        return match[1];
    }else{
        return null;
    }
}

/**
 * @callback pasteCallback
 * @param {string} link
 */

/**
 *
 * @param {string} text
 * @param {string} title
 * @param {pasteCallback} cb
 * @param {number} expiration
 * @param {boolean} private_
 * @param {string} author
 */
function paste(text, title, cb, expiration = 0, private_ = false, author = 'Sir Govan'){
    let body = `text=${encodeURIComponent(text)}&name=${encodeURIComponent(author)}&title=${encodeURIComponent(title)}`;
    if(expiration){
        body += `&expire=${encodeURIComponent(expiration)}`;
    }
    if(private_){
        body += `&private=1`;
    }
    let options = {
        headers: {'content-type': 'application/x-www-form-urlencoded'},
        url: "http://paste.sergiovan.me/api/create",
        body: body
    };

    request.post(options, function(err, response, body){
        if(!err && body && body.length < 400){
            cb(body.replace(/\n$/, '').replace('view/', 'view/raw/'));
        }else{
            Logger.error(`Response: ${response.statusCode}`);
            Logger.error(err);
            //TODO Handle this
        }
    });
}

/**
 * @callback fileUploadCallback
 * @param {string} err
 * @param {object} response
 * @param {string} body
 * */

/**
 *
 * @param {string} filename
 * @param {fileUploadCallback} cb
 */
function fileUpload(filename, cb){
    fs.readFile(path.join('logs', 'unfinished', filename), 'utf8', function(err, data){
        if(err){
            Logger.error(err);
            //TODO Handle
        }else{
            let date = filename.substring(0, filename.lastIndexOf('-')).replace(/\-/g, '/');
            let options = {
                headers: {'content-type' : 'application/x-www-form-urlencoded'},
                url: "http://sergiovan.me/chatlogs/upload.php",
                body: `key=${secrets.sergio.upload_key}&text=${encodeURIComponent(data)}&date=${date}&name=${filename}`
            }; //TODO Fix website, period
            request.post(options, cb);
        }
    });
}

/**
 * @callback uploadCallback
 * @param {boolean} success
 */

/**
 *
 * @param {uploadCallback} cb
 */
function upload(cb = (()=>{})){
    if(beta){
        Logger.status('Beta, not uploading!');
        cb(true);
    }else{
        glob('logs/unfinished/*.txt', null, function(err, files){
            if(err){
                Logger.error(err);
                cb(false);
                //TODO Handle
            }else{
                if(files.length === 0){
                    Logger.status('Nothing to upload');
                    return;
                }
                async.each(files, function(file, asyncCallback){
                    let filename = file.substring(file.lastIndexOf('/') + 1);
                    fileUpload(filename, function(err, response, body){
                        if(err){
                            Logger.log(err);
                            //TODO Handle error. For now though...
                            asyncCallback(true);
                        }else{
                            try{
                                body = JSON.parse(body);
                            }catch(JSONerr){
                                Logger.error(`Error parsing JSON after file ${file} was uploaded: ` +
                                             `"${body.substring(50)}${body.length > 50 ? '...' : ''}" is not valid JSON`);
                                Logger.debug(body);
                                asyncCallback(true);
                                return;
                            }
                            if(body.error){
                                Logger.error(`Error during upload of ${file}: ${body.message}`);
                                asyncCallback(true);
                            }else{
                                if(filename === (Logger.datestamp(true) + '.txt')){
                                    Logger.info(`${filename} uploaded correctly, not moved`);
                                    asyncCallback();
                                    return;
                                }
                                fs.rename(file, file.replace('unfinished', 'uploaded'), function(err){
                                    if(err){
                                        Logger.err(`Error while moving file: ${err}`);
                                        asyncCallback(true);
                                    }else{
                                        Logger.info(`${filename} uploaded correctly`);
                                        asyncCallback();
                                    }
                                });
                            }
                        }
                    });
                }, function(err){
                    Logger.info('File upload finished');
                    cb(!err);
                });
            }
        });
    }
}

/**
 * Creates a
 * @param {Function} funct
 * @returns {wrapped}
 */
function createBindeable(funct){
    function wrapped(...args){
        if(args.length < funct.length){
            return wrapped.bind(null, ...args);
        }else{
            return funct(...args);
        }
    }
    return wrapped;
}

/**
 *
 * @param {Function} funct
 * @returns {wrapper}
 */
function createBindeableWrapper(funct){
    function wrapper(a) {
        function wrapped(...args) {
            if(a instanceof Function) {
                let rett = a(...args);
                return rett instanceof Function ? wrapper(rett) : funct(rett);
            }else{
                return funct(a);
            }
        }
        return wrapped;
    }
    return wrapper;
}



//endregion

//region Process
process.on('uncaughtException', function(err){
    try{
        Logger.error(err.stack || err);
        //TODO Assess error, save and quit
    }catch(supererror){
        console.error(err.stack || err, supererror.stack || supererror);
        abort();
    }
});

process.on('exit', function(){
    Logger.debug('Weeeeee!');
    process.exit(0);
});

process.on('SIGINT', function(){
    process.exit(0);
});
//endregion

//region Classes

//region Functional Static "Class"

/** @namespace Functional */
const Functional = {
    lt: createBindeable((a,b) => a > b),
    gt: createBindeable((a,b) => a < b),
    eq: createBindeable((a,b) => a == b),
    eqq: createBindeable((a,b) => a === b),
    not: createBindeableWrapper((a) => !a),
    true: () => true,
    false: () => false,
    value: (a) => () => a,
    empty: () => {},
    nearly: createBindeable(closestError)
};

//endregion

//region Logger Static Class

class Logger {
    static printMessage(message, ...channels) {
        message = Logger.timestamp() + util.format(message);
        message = message.replace(/(\n|\r|\r\n)/g, '$&' + ' '.repeat(Logger.timestamp().length));
        if (channels.includes(Logger.stderr)) {
            console.error(message);
        } else {
            console.log(message);
        }
        if (channels.includes(Logger.public)) {
            io.broadcast(message);
            Logger.writeToLog(message);
        }
        if(channels.includes(Logger.guivan)){
            io.broadcast(message);
        }
        if (channels.includes(Logger.superuser)) {
            io.messageSuperUsers(message);
        }
        if (channels.includes(Logger.admin)) {
            io.messageAdmin(message);
        }
        if (!channels.includes(Logger.silent)) {
            Logger.writeToDebugLog(message);
        }
    }

    /**
     * Logs normally
     * @param {String...} message
     */
    static log(...message) {
        Logger.printMessage(util.format(...message), Logger.public);
    }

    /**
     * Logs bot status, which is important enough to go in the chat logs
     * @param {String...} message
     */
    static status(...message) {
        Logger.printMessage(colors.bgBlue.white(util.format(...message)), Logger.public);
    }

    /**
     * Logs useful info that is not important for the chat logs
     * @param {String...} message
     */
    static info(...message){
        Logger.printMessage(colors.bgBlue.white(util.format(...message)), Logger.guivan);
    }

    /**
     * Logs console users info, in green
     * @param {String...} message
     */
    static console(...message){
        Logger.printMessage(colors.green(util.format(...message)), Logger.guivan);
    }

    static hidden(...message) {
        Logger.printMessage('(hidden) ' + util.format(...message), Logger.superuser);
    }

    static debug(...message) {
        Logger.printMessage(util.format(...message), Logger.admin, Logger.silent);
    }

    static error(...message) {
        Logger.printMessage(colors.bgRed.white(util.format(...message)), Logger.superuser, Logger.stderr);
    }

    static writeToLog(message) {
        Logger.logStream.write(message.replace(
                /[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g, ''
            ) + '\n');
    }

    static writeToDebugLog(message) {
        Logger.debugLogStream.write(message.replace(
                /[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g, ''
            ) + '\n'); //Log stripped of color
    }

    static timestamp() {
        let date = new Date();
        let hours = '' + date.getHours();
        let minutes = '' + date.getMinutes();
        let seconds = '' + date.getSeconds();
        let millis = '' + date.getMilliseconds();
        return `${hours.length === 1 ? '0' + hours : hours}:${minutes.length === 1 ? '0' + minutes : minutes}` +
            `:${seconds.length === 1 ? '0' + seconds : seconds}.${'0'.repeat(3 - millis.length) + millis} - `;
    }

    static datestamp(file = false) {
        let date = new Date();
        let year = '' + date.getFullYear();
        let month = '' + (date.getMonth() + 1);
        let day = '' + date.getDate();
        if (file) {
            return `${year}-${month.length === 1 ? '0' + month : month}-${day.length === 1 ? '0' + day : day}`
        } else {
            return `${year}/${month.length === 1 ? '0' + month : month}/${day.length === 1 ? '0' + day : day}`
        }
    }

    /* Public channel: Sent to everyone with a GUIvan */
    static get public() {
        if (!Logger._public) {
            Logger._public = Symbol();
        }
        return Logger._public;
    }

    static get guivan() {
        if(!Logger._guivan){
            Logger._guivan = Symbol();
        }
        return Logger._guivan;
    }

    /* Silent channel: Not logged to Logger.debugLogStream */
    static get silent() {
        if (!Logger._silent) {
            Logger._silent = Symbol();
        }
        return Logger._silent;
    }

    /* Superuser channel: Sent to all authenticated GUIvan users */
    static get superuser() {
        if (!Logger._superuser) {
            Logger._superuser = Symbol();
        }
        return Logger._superuser;
    }

    /* Admin channel: Sent to the Admin */
    static get admin() {
        if (!Logger._admin) {
            Logger._admin = Symbol();
        }
        return Logger._admin;
    }

    /* Error channel: Logged on stderr instead of stdout */
    static get stderr() {
        if (!Logger._error) {
            Logger._error = Symbol();
        }
        return Logger._error;
    }

    static get logStream() {
        if (!Logger._logStream) { //No log stream
            let file = Logger.datestamp(true);
            try {
                fs.accessSync(path.join('logs', 'unfinished'), fs.W_OK); //Cannot be done the other way if I want to return
                Logger._logStream = fs.createWriteStream(path.join('logs', 'unfinished', file + '.txt'), {flags: 'a'});
                Logger._logFileDate = file;
            } catch (err) { //Directory does not exist
                try {
                    mkdirp.sync(path.join('logs', 'unfinished')); //Damn you synchronicity
                    mkdirp.sync(path.join('logs', 'uploaded'));
                    Logger._logStream = fs.createWriteStream(path.join('logs', 'unfinished', file + '.txt'), {flags: 'a'});
                    Logger._logFileDate = file;
                } catch (err) {
                    console.error(Logger.timestamp() + colors.bgRed.white(err.stack));
                    abort();
                }
            }
            Logger._logStream.write('~~~ ~~~ ~~~\n');
            return Logger._logStream; //At this point it was created, or everything crashed and died
        } else {
            if (Logger._logFileDate === Logger.datestamp(true)) {
                return Logger._logStream;
            } else {
                let file = Logger.datestamp(true);
                Logger._logStream.end();
                Logger._logStream = fs.createWriteStream(path.join('logs', 'unfinished', file + '.txt'), {flags: 'a'});
                Logger._logFileDate = Logger.datestamp(true);
                Logger.printMessage(Logger.datestamp(), Logger.public, Logger.silent);
                upload();
                Logger._logStream.write('~~~ ~~~ ~~~\n');
                return Logger._logStream;
            }
        }
    }

    static get debugLogStream() {
        if (!Logger._logDebugStream) {
            try {
                fs.accessSync('data', fs.W_OK); //Directory exists
                Logger._logDebugStream = fs.createWriteStream(path.join('data', 'log'), {flags: 'a'});
                Logger._logDebugDate = Logger.datestamp(true);
                Logger._logDebugStream.write(`~~~ ${Logger.datestamp()} ~~~\n`);
            } catch (err) { //Directory does not exist
                try {
                    mkdirp.sync('data');
                    Logger._logDebugStream = fs.WriteStream(path.join('data', 'log'), {flags: 'a'});
                    Logger._logDebugDate = Logger.datestamp(true);
                    Logger._logDebugStream.write(`~~~ ${Logger.datestamp()} ~~~\n`);
                } catch (err) {
                    console.error(Logger.timestamp() + colors.bgRed.white(err.stack));
                    abort();
                }
            }
            Logger._logDebugDate = Logger.datestamp();
            return Logger._logDebugStream;
        } else {
            if (Logger._logDebugDate === Logger.datestamp()) {
                return Logger._logDebugStream;
            } else {
                Logger._logDebugStream.write(`~~~ ${Logger.datestamp()} ~~~\n`);
                Logger._logDebugDate = Logger.datestamp();
                return Logger._logDebugStream;
            }
        }
    }
}

//endregion

//region Persistent Class

class Persistent {
    constructor(filename, obj = {}, reviver = false){
        if(!filename){
            throw new Error("Persistent data needs a file to be stored onto");
        }
        this._filename = filename;
        this.obj = obj;
        try {
            fs.accessSync(path.join('data', filename), fs.R_OK | fs.W_OK);
            let nobj = JSON.parse(fs.readFileSync(path.join('data', filename)).toString(), reviver);
            if(Object.keys(obj).length !== 0){
                onlyThis(nobj, obj);
            }
            this.obj = nobj;
        }catch(err){ //File does not exist, error in reviver
            if(err.code === 'ENOENT'){
                this.save();
            }else if(err.message.indexOf('JSON') > -1){
                this.obj = {};
                fs.renameSync(path.join('data', filename), path.join('data', `${filename}-${new Date().getTime()}`));
            }
        }

        return new Proxy(this, {
            get: function(target, name){
                if(name in target){
                    return target[name];
                }else{
                    return target.obj[name];
                }
            },
            set: function(target, name, value){
                target.obj[name] = value;
                return true;
            }
        });
    }


    save(cb = (()=>{})) {
        fs.writeFile(path.join('data', this._filename), JSON.stringify(this.obj, null, 4), (err) => {
            if(err){
                Logger.error(err.stack);
                Logger.hidden(JSON.stringify(this.obj, null, 4));
            }else{
                Logger.debug("File saved in " + path.join('data', this._filename));
                cb();
            }
        });
    }

}

//endregion

//region BotData class

class BotData {
    constructor(){
        this.mode = BotData.modes.normal;

        this.voting = false;
        this.timeout = {};
        this.yesVotes = 0;
        this.noVotes = 0;
        this.votesNeeded = 0;
        this.votesGotten = 0;
        this.voters = {};

        this.named = false;
        this.chat = beta ? secrets.steam.betaChat : secrets.steam.mainChat;
        this.imson = null;
        this.persistent = new Persistent('persistent.txt', {
            counter: 0,
            runs: 0
        });
        this.grantStreak = 0;
        this.ghostTimer = -1;
    }

    /**
     * Callback for voting
     * @callback voteFunction
     * @param {Boolean} passed If the vote passed or not
     * */

    /**
     * Starts a vote
     * @param {Number} needed Votes needed to pass
     * @param {voteFunction} funct Function to execute at the end of the vote
     * @param {Number} time Vote time in seconds
     */
    startVote(needed, funct, time = 120){
        if(this.voting){
            throw new Error("Vote already in progress, dummy");
        }
        if(typeof needed !== 'number'){
            throw new Error("Vote needs a threshold!");
        }
        if(!(funct instanceof Function)){
            throw new Error("Vote needs a valid function to end");
        }
        this.voting = true;
        this.votesNeeded = needed;
        this.timeout = setTimeout(this.endVote.bind(this, funct), time * 1000);
        this.timeout.startMillis = new Date().getTime();
    }

    /**
     * Vote for an ongoing vote
     * @param {User} voter User voting
     * @param {String} vote Their vote
     */
    vote(voter, vote){
        if(!this.voting){
            throw new Error("Cannot vote without a vote going on");
        }
        if(![BotData.votes.yes, BotData.votes.no, BotData.votes.whatever].includes(vote)){
            throw new Error("Illegal vote, call the police immediately: " + vote);
        }
        if(this.voters[voter.id] === undefined){
            voter.superSecretTell("You may not cast a vote on the current ongoing vote");
        }else if(this.voters[voter.id]){
            voter.superSecretTell(S.alreadyVoted);
        }else{
            voter.superSecretTell("Vote added to total count");
            this.voters[voter.id] = true;
            this.votesGotten++;
            switch(vote){
                case BotData.votes.yes:
                    this.yesVotes++;
                    break;
                case BotData.votes.no:
                    this.noVotes++;
                    break;
            }
        }
    }

    endVote(funct){
        if(this.votesGotten >= this.votesNeeded){
            if(this.yesVotes > this.noVotes){
                funct(true);
            }else{
                funct(false);
            }
        }else{
            funct(false);
        }
        this.terminateVote();
    }

    terminateVote(){
        this.voting = false;
        this.yesVotes = this.noVotes = this.votesNeeded = this.votesGotten = 0;
        this.voters = {};
        clearTimeout(this.timeout);
    }

    static get universalVote(){
        return new Proxy({}, {
            get(target, name){
                if(target[name] !== undefined){
                    return target[name];
                }else{
                    return false;
                }
            }
        });
    }

    /**
     *
     * @returns {{normal: number, dalek: number, silent: number}}
     */
    static get modes(){
        return {
            normal: 0,
            dalek: 1,
            silent: 2
        };
    }

    static get votes(){
        return {
            yes: 'y',
            no: 'n',
            whatever: 'w'
        }
    }
}

//endregion

//region ExternalData Class

/**
 * For functions and data from files outside Govan that are not persistent
 */
class ExternalData {
    static get jokeFile(){
        return path.join('data', 'jokes.txt');
    }

    static get joke(){
        let jokes = readOrCreate(ExternalData.jokeFile, (jokes) => jokes.split(/\r\n|\r|\n/));
        return jokes.randomElement() || 'I knew a very good one, but I forgot';
    }

    static addJoke(joke){
        if(typeof joke !== 'string'){
            throw new Error('Joke is not a string! Is this some kind of joke?');
        }
        if(joke === ''){
            throw new Error('Joke is empty! How unfunny...');
        }
        let stream = fs.createWriteStream(ExternalData.jokeFile, {flags: 'a'});
        stream.write(joke + '\n');
        stream.end();
    }

    static get wittyFile(){
        return path.join('data', 'witty.txt');
    }

    static get witty(){
        let witty = readOrCreate(ExternalData.wittyFile, (wittys) => wittys.split(/\r\n|\r|\n/));
        return witty.randomElement() || 'What?';
    }

    static get attributesFile(){
        return path.join('data', 'attributes.txt');
    }

    static get attributes(){
        return readOrCreate(ExternalData.attributesFile, (attr) => attr.split(/\r\n|\r|\n/));
    }

    static get forbiddenFile(){
        return path.join('data', 'forbidden.txt');
    }

    /**
     * Censors a message based on forbidden words/links
     * @param {String|Array} message The message to censor
     * @returns {String} The censored message
     */
    static censor(message){
        if(typeof message === 'string'){
            message = message.split(/ |\r\n|\r|\n/).filter(str => str !== '');
        }
        let forbidden = readOrCreate(ExternalData.forbiddenFile, (file) => file.split(/\r\n|\r|\n/));
        for(let i = 0; i < message.length; i++){
            if(forbidden.includes(message[i])){
                message[i] = '[REMOVED]';
            }
        }
        return message.join(' ');
    }
}

//endregion

//region Searchable Superclass

/**
 * This function is called by search() to search, to be specified by each child separately
 * @callback search
 * @param {String} term The term we're searching for
 * @returns {Array} rett Code and term found
 */

/**
 * This function is called by search() to validate a cache entry, to be specified by each child separately
 * @callback validateCacheEntry
 * @param {String} term The term that was searched for
 * @param {Object} found The object previously found with that term
 * @returns {Boolean} If term and found are still related correctly
 */

/**
 * @class Searchable
 */
class Searchable {
    /**
     * Constructor for Searchable
     * @param {search} fn Search function
     * @param {validateCacheEntry} validate Validation function
     */
    constructor(fn, validate){
        this._cache = {};
        this._search = fn;
        this._validate = validate;
    }

    /**
     *
     * @param {String|Number} term
     * @param {search} search
     * @param {validateCacheEntry} validate
     * @returns {*}
     */
    search(term, search = this._search, validate = this._validate){
        if(this._cache.hasOwnProperty(term)){
            if(validate.call(this, term, this._cache[term])) {
                return [Searchable.codes.found, this._cache[term]];
            }else{
                delete this._cache[term];
            }
        }
        let code = 0, obj = null;
        [code, obj] = search.call(this, term);
        if(code > 0){ //Found or unique
            this._cache[term] = obj;
            return [code, obj];
        }else{
            return [code, null];
        }
    }

    /**
     *
     * @returns {{unique: number, found: number, not_found: number, too_many: number}}
     */
    static get codes(){
        return {
            unique: 2,
            found: 1,
            not_found: 0,
            too_many: -1
        }
    }
}

//endregion

//region Superclass UserDataContainer

class UserDataContainer {
    /**
     *
     * @param {User} parent
     * @returns {UserDataContainer}
     */
    constructor(parent){
        if(!parent || !(parent instanceof User)){
            throw new Error('Parent is not a user: ' + parent);
        }
        this._parent = {
            parent: parent,
            toJSON(){
                return undefined;
            }
        };
        Object.defineProperties(this, {
            '_parent': {enumerable: false}
        });
    }

    /**
     *
     * @returns {User}
     */
    get parent(){
        return this._parent.parent;
    }
}

//endregion

//region Userdata

//region Users

class Users extends Searchable {
    /**
     *
     */
    constructor(){
        function _search(term){
            if(isUserID(term) && this._users[term]){
                return [Searchable.codes.unique, this._users[term]];
            }
            let closeness = Number.POSITIVE_INFINITY, found = [], isName = false;
            let naive = -1, naiveName = false;
            for(let userID in this._users.obj){
                if(this._users.obj.hasOwnProperty(userID)){
                    let user = this._users[userID];
                    if(this._users[userID].name === 'term'){
                        return [Searchanle.codes.unique, user];
                    }
                    let distName = wordDistance(user.name, term);
                    distName = closestError(user.name, term, distName) ? distName : Number.POSITIVE_INFINITY;
                    let distAlias = user.alias ? wordDistance(user.alias, term) : Number.POSITIVE_INFINITY;
                    distAlias = closestError(user.alias, term, distAlias) ? distAlias : Number.POSITIVE_INFINITY;
                    if(distAlias < Number.POSITIVE_INFINITY){
                        if(!isName && distAlias === closeness){
                            found.push(user);
                        }else if(distAlias < closeness){
                            found = [user];
                            isName = false;
                            closeness = distAlias;
                        }
                    }
                    if(distName < Number.POSITIVE_INFINITY){
                        if(!isName && distName === closeness){
                            found = [user];
                            isName = true;
                        }else if(distName === closeness){
                            found.push(user);
                        }else if(distName < closeness){
                            found = [user];
                            isName = true;
                            closeness = distName;
                        }
                    }
                    if(!naiveName && naive === -1 && user.alias.indexOf(term) > -1){
                        naive = user;
                    }
                    if(!naiveName && naive !== -1 && user.alias.indexOf(term) > -1){
                        naive = -1;
                        naiveName = true;
                    }
                    if((naive === -1 || naiveName === false) && user.name.indexOf(term) > -1){
                        naive = user;
                        naiveName = true;
                    }else if((naive !== -1 && naiveName === true) && user.name.indexOf(term) > -1){
                        naive = null;
                        naiveName = true;
                    }
                }
            }
            if(naive !== -1 && naive !== null){
                return [Searchable.codes.found, naive];
            }
            if(found.length === 0){
                return [naive === null ? Searchable.codes.too_many : Searchable.codes.not_found, null];
            }else if(found.length > 1){
                return [Searchable.codes.too_many, null];
            }else{
                return [Searchable.codes.found, found[0]];
            }
        }

        function _validate(term, obj){
            return closestError(term, obj.name) || (closestError(term, obj.alias) && obj.alias);
        }

        super(_search, _validate);
        /** @type {User[]|Persistent} */
        this._users = new Persistent('userData.dat');
        this._convert();
        this._purge();

        return new Proxy(this, {
            get(target, name){
                if(isUserID(name)){
                    return target._users[name];
                }else{
                    return target[name];
                }
            }
        });
    }

    /**
     *
     * @param {String} id
     * @param {String} name
     */
    addUser(id, name=''){
        if(!this._users[id]){
            this._users[id] = new User(id, name);
            if(id === stclient.steamID){
                this._users[id].banned = true;
            }
            Logger.debug(`Added ${name}`);
            this._users.save();
        }else{
            this._users[id].name = name;
        }
    }

    /**
     *
     * @returns {User[]}
     */
    get userArray(){
        let arr = [];
        for(let userID in this._users.obj){
            if(this._users.obj.hasOwnProperty(userID)){
                arr.push(this._users.obj[userID]);
            }
        }
        return arr;
    }

    _convert(){
        for(let userID in this._users.obj){
            if(this._users.obj.hasOwnProperty(userID)){
                let user = this._users[userID];

                let newUser = new User(user.id);
                for(let prop in user){
                    if(user.hasOwnProperty(prop)){
                        if(prop === 'subscriptions'){
                            newUser[prop] = new Subscriptions(newUser, user[prop]);
                        }else if(prop === 'murder'){
                            newUser[prop] = new Murder(newUser, user[prop]);
                        }else if(prop === 'curses'){
                            newUser[prop] = new Murder(newUser, user[prop]);
                        }else if(prop === 'rpg'){
                            newUser[prop] = new RPG(newUser, user[prop]);
                        }else if(prop === 'lastLines'){
                            if(user[prop].length !== 4){
                                let lastLength = user[prop].length;
                                user[prop].length = 4;
                                user[prop].fill('', lastLength);
                                newUser[prop] = user[prop];
                            }
                        }else{
                            newUser[prop] = user[prop];
                        }
                    }
                }
                this._users[userID] = newUser;
            }
        }
    }

    _purge(){
        for(let index in this._users.obj){
            if(this._users.obj.hasOwnProperty(index)){
                let user = this._users[index];
                if(user.trusted || user.banned || user.superuser || user.untrusted || user.admin){ //Skip trusted or banned users, they're too important
                    continue;
                }
                if(user.lastSpoke === -1){
                    Logger.debug(`${user.name} removed due to silence`);
                    delete this._users.obj[index];
                }else if(user.lastSpoke + 2628000000 < new Date().getTime()){
                    Logger.debug(`${user.name} removed due to 1 month away`);
                    delete this._users.obj[index];
                }else if(user.rpg.totalExperience < 1){
                    Logger.debug(`${user.name} removed due to lack of XP`);
                    delete this._users.obj[index];
                }
            }
        }
    }

    pasteAll(place){
        let sorted = this.userArray;
        sorted = sorted.filter((u) => u.rpg.totalExperience > 0);
        sorted.sort(function(a, b){
            return b.rpg.level - a.rpg.level ||
                   b.rpg.experience - a.rpg.experience ||
                   b.rpg.totalExperience - a.rpg.totalExperience ||
                   (a.parent.name < b.parent.name ? -1 : 1);
        });

        const INFO = ` --- INFO --- \n`;
        let info = INFO;
        for(let user of sorted){
            info += tagRemoveIndentation
                `${user.rpg.prefix} ${user.rpg.race} ${user.rpg.class} ${user.name}
                 Level: ${user.rpg.level}
                 XP: ${user.rpg.experience}
                 XP until next level: ${RPG.expFunction(user.rpg.level + 1) - user.rpg.experience}
                 Total accumulated XP: ${user.rpg.totalExperience}
                 --------------\n`;
        }

        info = info === INFO ? 'Everyone died in that tragic accident...' : info;
        paste(info, `Everyone's info`, function(link){
            bot.sendMessage(place, link);
        });
    }

    static active(){
        return (id) => {
            return  id !== stclient.steamID &&
                users._users.obj.hasOwnProperty(id) &&
                users[id].subscriptions.grant &&
                users[id].rpg.totalExperience > 0 &&
                (new Date().getTime() - users[id].lastSpoke < 604800000) && //Spoke in the last 15 days
                !users[id].banned;
        }
    }

    static currentActive(){
        return (id) => {
            let user = users[id];
            return user && !user.banned && user.id !== stclient.userID &&
                new Date().getTime() - users[id].lastSpoke < (1000 * 60 * 5);
        }
    }

}

//endregion

//region User

class User {
    /**
     *
     * @param {String} id
     * @param {String} name
     */
    constructor(id, name = ''){
        if(!id){
            throw new Error('User initialized without id');
        }
        if(!isUserID(id)){
            throw new Error(`ID provided (${id}) is not a SteamID`);
        }

        //Identification
        this.id = id;
        this.name = name;
        this.alias = '';

        //Reminders
        this.lastLines = ['','','',''];
        this.lastSpoke = -1;
        this.lastLeft = -1;
        this.lastVersion = '';
        this.attention = false;
        this.nextSecret = false;
        this.nextSuperSecret = false;

        //Status
        this.banned = false;
        this.superuser = false;
        this.admin = false;
        this.trusted = false;
        this.untrusted = false;

        //Messages
        this.pending = '';

        //Other
        this.subscriptions = new Subscriptions(this);
        this.murder = new Murder(this);
        this.curses = new Curse(this);
        this.rpg = new RPG(this);
    }

    get rpgname(){
        if(this.subscriptions.get('ultrasilence')){
            return 'That one guy';
        }
        let name = '';
        if(this.rpg.prefix){
            name = this.rpg.prefix + ' ';
        }
            
        if(randInt(30) === 0){
           name += `${this.rpg.race} ${this.rpg.class} `;
        }
        return (name + this.name);
    }

    /**
     *
     * @param {String} message
     * @param {boolean} start
     */
    chainTell(message, start = false){
        if(start){
            this.pending = '' + message;
        }else{
            this.pending += message;
        }
    }

    chainSend(message = ''){
        this.pending += message;
        if(this.pending){
            this.tell(this.pending);
            this.pending = '';
        }
    }

    chainClear(){
        this.pending = '';
    }

    isTrusted(){
        return this.trusted || this.isSuperUser();
    }

    isSuperUser(){
        return this.superuser || this.isAdmin();
    }

    isAdmin(){
        return this.admin || this.id === secrets.steam.ownerID;
    }

    get tell(){
        return bot.sendMessage.bind(bot, this.id);
    }

    get secretTell(){
        return bot.sendMessage.placeholder(bot, this.id, _, true);
    }

    get superSecretTell(){
        return bot.sendMessage.placeholder(bot, this.id, _, true, true);
    }
}

//endregion

//region RPG


class RPG extends UserDataContainer {
    constructor(parent, obj = {}){
        super(parent);
        this.experience = 0;
        this.totalExperience = 0;
        this.level = 0;
        this.maxLevel = 0;

        this.lastGift = -1;
        this.lastPerson = '';

        this.prefix = '';
        this.class = 'Procrastinator';
        this.race = 'Human';

        this.attributes = {};
        this.bought = {};

        this.hourglasses = 0;
        this.firesInAJar = 0;

        fuse(this, obj);
    }

    /**
     *
     * @param {number} amount
     * @param {User} granter
     * @param {boolean} free
     * @param {boolean} all
     */
    grant(amount, granter, free = false, all = false){
        if(!this.parent.subscriptions.get('grant')){
            granter.tell(`That user would rather not be granted anything`);
            return;
        }
        if(amount <= 0 || !Number.isInteger(amount)){
            throw new Error('Granting illegal amount of XP: ' + amount);
        }
        let paidAmount = amount;
        if(this.parent.curses.derpy){
            amount = tenize(amount, this.parent.curses.derpy);
        }
        if(granter === null){
            this.parent.chainTell(`You have gained ${amount} XP! `);
        }else{
            if(granter.curses.govan){
                amount = tenize(amount, granter.curses.govan);
            }
            if(granter.rpg.experience < amount && !free){
                granter.tell(`You don't have that much XP to give! You only have ${granter.rpg.experience}`);
                return;
            }else if(granter.rpg.experience >= amount && !free && !all){
                this.parent.chainTell(`${granter.rpgname} has given you ${amount} XP! `);
                granter.chainTell(`You have given ${this.parent.name} ${paidAmount} XP. `);
                granter.rpg.payXP(paidAmount);
            }else if(granter.rpg.experience >= amount && !free && all){
                this.parent.chainTell(`${granter.rpgname} has given you all of his XP (${amount})! `);
                granter.chainTell(`You have given ${this.parent.name} all of your XP (${paidAmount}). `);
                granter.rpg.payXP(paidAmount);
            }else if(free){
                this.parent.chainTell(`${granter.rpgname} has gifted you ${amount} XP! `);
                granter.chainTell(`You have gifted ${this.parent.name} ${paidAmount} XP. `);
            }
        }
        let lastLevel = this.level;
        this.experience += amount;
        this.totalExperience += amount;
        this.parent.chainSend(`You now have ${this.experience} XP`);
        if(granter !== null){
            granter.chainSend(`${S.pronoun} now has ${this.experience} XP`);
        }else{
            granter.chainSend();
        }
        this.level = RPG.checkLevel(this.experience);
        if(lastLevel !== this.level){ //Level up
            let info = '';
            if(this.level <= this.maxLevel){
                info += 'No attributes gained\n';
            }else{
                let levelAmount = this.maxLevel - this.level, attrAmount = 0;
                this.maxLevel = this.level;
                attrAmount += randInt(5 * levelAmount) + 5 * levelAmount;
                let attributes = ExternalData.attributes;
                if(!attributes[0]){
                    for(let value of ['Attack', 'Defense', 'Magic', 'Luck']) {
                        info += `${value} +${randInt(1, 6)}!\n`;
                    }
                    attrAmount = 0;
                }
                let upgrades = {};
                for(let i = 0; i < attrAmount; i++){
                    let attrName = attributes.randomElement();
                    let upgradeAmount = randInt(1, 6);
                    if(this.attributes[attrName] === undefined){
                        this.attributes[attrName] = 0;
                    }
                    this.attributes[attrName] += upgradeAmount;
                    if(upgrades[attrName] === undefined){
                        upgrades[attrName] = 0;
                    }
                    upgrades[attrName] += upgradeAmount;
                }
                for(let attrName in upgrades){
                    if(upgrades.hasOwnProperty(attrName)){
                        info += `${attrName} +${upgrades[attrName]}!\n`;
                    }
                }
            }
            let self = this;
            this.pasteInfo(bot.botData.chat, info);
        }
    }

    /**
     *
     * @param {number} amount
     * @param {boolean} forced
     * @returns {boolean}
     */
    payXP(amount, forced = false){
        if(amount <= 0 || !Number.isInteger(amount)){
            throw new Error('Paying illegal amount of XP: ' + amount);
        }

        if(forced){
            amount = Math.min(amount, this.experience);
        }else if(amount > this.experience){
            return false;
        }

        this.experience -= amount;
        this.level = RPG.checkLevel(this.experience);
        return true;
    }

    payLevels(amount, forced = false){
        if(amount <= 0 || !Number.isInteger(amount)){
            throw new Error('Paying illegal amount of levels: ' + amount);
        }

        if(forced){
            amount = Math.min(amount, this.level);
        }else if(amount > this.level){
            return false;
        }

        this.level -= amount;
        this.experience = RPG.expFunction(this.level);
        return true;
    }

    pasteInfo(place, levelupinfo = false){
        let name = this.parent.name;
        let info =
            tagRemoveIndentation`${Logger.datestamp()} ${Logger.timestamp().slice(0, -7)}
                                 ${this.prefix} ${this.race} ${this.class} ${name}
                                 Level: ${this.level}
                                 XP: ${this.experience}
                                 XP until next level: ${RPG.expFunction(this.level + 1) - this.experience}
                                 Total accumulated XP: ${this.totalExperience}`;
        if(levelupinfo){
            info += `Leveled up!\n${levelupinfo}`;
        }else{
            let curses = this.parent.curses;
            const INFO = '\n--- INFO ---\n\n';
            const ATTR = '\n--- ATTRIBUTES ---\n\n';
            info += INFO;
            info += this.bought.lore ? this.bought.lore + '\n' : '';
            info += this.firesInAJar ? `Fires in a jar: ${this.firesInAJar}, giving a ${this.firesInAJar}% boost` +
            ` in XP gifted\n` : '';
            info += this.hourglasses ? `Magic hourglasses: ${this.hourglasses}, removing ` +
            `${Math.min(this.hourglasses, 600)} seconds between XP gifts\n` : '';
            info += curses.curseAmount > 5 ? `Dark shadows loom over ${name}\n` : '';
            info += curses.govan ? `${name} has a curse of the Govan of strength ${curses.govan}\n` : '';
            info += curses.timezone ? `${name} has a curse of the Wrong Timezone of strength ${curses.timezone}\n` : '';
            info += curses.blind ? `${name} has a curse of the Blind\n` : '';
            info += curses.amnesia ? `${name} has a curse of Amnesia of strength ${curses.amnesia}\n` : '';
            info += curses.illiteracy ? `${name} has a curse of Illiteracy\n` : '';
            info += curses.derpy ? `${name} has a curse of Derpy of strength ${curses.derpy}\n` : '';
            info += ((info.indexOf(INFO) + INFO.length) === info.length ? 'Nothing to see here...\n' : '');
            info += ATTR;
            let attrs = [];
            for(let attribute in this.attributes){
                if(this.attributes.hasOwnProperty(attribute)){
                    attrs.push([attribute, this.attributes[attribute]]);
                }
            }
            attrs.sort((a, b) => (b[1] - a[1]) ? b[1] - a[1] : (a[0] > b[0] ? 1 : -1));
            for(let attr of attrs){
                info += `${attr[0]}: ${attr[1]}\n`;
            }
            info += ((info.indexOf(ATTR) + ATTR.length) === info.length ? 'This person has no attributes...\n' : '');
        }
        let title = levelupinfo ? `${name} leveled up to level ${this.level}!` : `Info for ${name}`;
        paste(info, title, (link) => {
            bot.sendMessage(place, (levelupinfo ? `${name} leveled up to level ${this.level}! ` : 'Info: ') +
                link);
        })
    }

    /**
     * Returns amount of XP needed to reach level.
     * That is, if you have as much XP as this function returns, you're
     * level level.
     * @param {number} level
     * @returns {number}
     */
    static expFunction(level){
        return Math.floor(200*level + Math.pow(level, 2) + Math.pow(level, 3)/3);
    }

    /**
     * Returns the level someone is with exp experience.
     * That is, if you have exp experience you are the level returned
     * @param {number} exp
     * @returns {number}
     */
    static checkLevel(exp){
        exp++;
        let x2 = exp*exp;
        let croot2 = Math.pow(2, 1/3);
        let bigsqrt = Math.sqrt(9*x2+3588*exp+31880000);
        let crootbigsqrt = Math.pow(bigsqrt+3*exp+598, 1/3);
        return Math.ceil(crootbigsqrt/croot2-((199*croot2)/crootbigsqrt)-1) - 1;
    }
}

//endregion

//region Subscriptions

class Subscriptions extends UserDataContainer {
    constructor(parent, obj = {}){
        super(parent);
        this.grant = true;
        this.silence = false;
        this.ultrasilence = false;
        this.murder = true;
        this.messages = true;
        this.kick = true;
        this.ghost = false;

        fuse(this, obj);
    }

    /**
     *
     * @param {String} subscription
     * @returns {Boolean}
     */
    get(subscription){
        if(this[subscription] === undefined){
            throw new Error('Subscription does not exist: ' + subscription);
        }
        return this[subscription];
    }

    /**
     *
     * @param {String} subscription
     * @param {Boolean} value
     */
    set(subscription, value = true){
        if(this[subscription] === undefined){
            throw new Error('Subscription does not exist: ' + name);
        }
        if(typeof value !== 'boolean'){
            throw new Error(`Trying to set ${subscription} to non-boolean value: ${value}`);
        }
        this[subscription] = value;
    }
}

//endregion

//region Murder

class Murder extends UserDataContainer{
    constructor(parent, obj = {}){
        super(parent);
        this.murders = 0;
        this.deaths = 0;

        fuse(this, obj);
    }

    /**
     *
     * @param {User} killer
     */
    kill(killer){
        if(!(killer instanceof User)){
            throw new Error('Killer is not a user: ' + killer);
        }
        if(!this.parent.subscriptions.get('murder')){
            //TODO Messages
            return;
        }
        killer.murder.murders++;
        this.deaths++;
    }
}


//endregion

//region Curse

class Curse extends UserDataContainer{
    constructor(parent, obj = {}){
        super(parent);
        this.govan = 0;
        this.timezone = 0;
        this.derpy = 0;
        this.blind = 0;
        this.amnesia = 0;
        this.illiteracy = 0;

        this.curseAmount = 0;

        fuse(this, obj);
    }

    /**
     *
     */
    curse(){
        if(Math.random() < Curse.probability(this.curses.curseAmount)){
            let curse = Math.floor(Math.random() * 6);
            switch(curse){
                case 0: //Curse of the Govan
                    this.govan++;
                    break;
                case 1: //Curse of the Wrong Timezone
                    this.timezone++;
                    break;
                case 2: //Curse of the Derpy
                    this.derpy++;
                    break;
                case 3: //Curse of the Blind
                    this.blind++;
                    break;
                case 4: //Curse of Illiteracy
                    this.illiteracy++;
                    break;
                case 5: //Curse of Amnesia
                    this.amnesia++;
                    break;
            }
            this.parent.tell("You have been cursed!");
        }
    }

    hopeless(){
        return (this.derpy > 3 && this.amnesia > 0 && this.govan > 1) ||
            (this.govan > 2 && this.timezone > 0 && this.amnesia > 5 && this.derpy > 0);
    }

    clear(){
        this.amnesia = this.blind = this.derpy = this.govan = this.illiteracy = this.timezone = this.curseAmount = 0;
    }

    static probability(x){
        return Math.max(0, 1/((-x*0.1) - 1.5) + 0.5);
    }
}

//endregion

//endregion

//region Commander

class Constraint {
    constructor(funct, fail = Functional.empty){
        this.funct = funct;
        this.fail = fail;
    }

    test(...arg){
        return this.funct(...arg);
    }

    static isTrusted(){
        return new Constraint((u) => u.isTrusted(),
            () => "I don't trust you with that");
    }

    static isSuperUser(){
        return new Constraint((u) => u.isSuperUser());
    }

    static isAdmin(){
        return new Constraint((u) => u.isAdmin());
    }

    static notBanned(){
        return new Constraint((u) => !u.banned || u.isAdmin(), () => S.userNotFound);
    }
}

class ParseNode {
    constructor(type, name, {constraints = [], value = null, strict = true, ending = null} = {}){
        if(type === undefined){
            throw new Error("ParseNode needs a type!");
        }
        if(name === undefined){
            throw new Error("ParseNode needs a name!");
        }
        if(constraints instanceof Constraint){
            constraints = [constraints];
        }
        if(!(constraints instanceof Array)){
            throw new Error("Illegal constraints: " + constraints);
        }
        if(type === 0 && value === null){
            throw new Error("ParseNode of constant type needs a value!");
        }else if(type === 0){
            constraints = [strict ?
                           new Constraint(Functional.eqq(value), () => `Invalid parameter ${name}`) :
                           new Constraint(Functional.nearly(value), () => `Invalid parameter ${name}`)];
        }
        if(type === ParseNode.types.end && ending === null){
            throw new Error('End node without ending value!');
        }

        /** @type {Number} */
        this.type = type;

        /** @type {String} */
        this.name = name;

        /** @type {Constraint[]} */
        this.constraints = constraints;

        /** @type {ParseNode[]} */
        this.children = [];

        /** @type {ParseNode} */
        this.parent = null;

        /** @type {String} */
        this.value = value;

        /** @type {Number} */
        this.ending = ending;

        this[Symbol.iterator] = function*(){
            for(let value of this.children){
                yield value;
            }
        }
    }

    addChild(node){
        if(node === ParseNode._){
            node = this;
        }
        if(!(node instanceof ParseNode)){
            throw new Error("Cannot add a child that isn't a ParseNode: " + node);
        }
        this.children.push(node);
        this.children.sort((a, b) => {
            let comp = a.type - b.type;
            return comp ? comp : b.constraints.length - a.constraints.length;
        });
        return this;
    }

    isStartNode(){
        return this.parent === null;
    }

    isEndNode(){
        return this.children.length === 0 || this.type === ParseNode.types.end;
    }

    test(value, coerce = false){
        let [passed, fail] = [true, null];
        switch(this.type){
            case ParseNode.types.empty:
                if(!value){
                    return [true, null, null];
                }else{
                    return [false, () => `I expected nothing, and you still managed to dissapoint me`, value];
                }
            case ParseNode.types.constant:
                if(coerce){
                    value = closestError(value, this.value) ? this.value : value;
                }
                break;
            case ParseNode.types.user:
                let [code, user] = users.search(value); //MAGIC
                if(code === Searchable.codes.not_found || code === Searchable.codes.too_many){
                    [passed, fail] = [false,
                        code === Searchable.codes.not_found ? () => S.userNotFound :
                                () => S.tooManyUsers];
                }else{
                    value = user;
                }
                if(value.banned){
                    [passed, fail] = [false, () => `User is banned`];
                }
                break;
            case ParseNode.types.number:
                if(coerce && typeof value === 'string'){
                    let coerced = value.replace(/[^+\-0-9]*/g, '');
                    value = closestError(coerced, value, value.length - coerced.length) ? coerced : value;
                }
                if(!isNumber(value)){
                    [passed, fail] = [false, (u) => {u.tell(`${value} is not a number`)}];
                }
                value = +value;
                break;
            case ParseNode.types.string:
            case ParseNode.types.longString:
                break;
            case ParseNode.types.end:
                return [true, null, true];
            default:
                throw new Error("ParseNode has undefined type: " + this.type);
        }
        if(passed){
            for(let constraint of this.constraints){
                if(!constraint.test(value)){
                    [passed, fail] = [false, constraint.fail];
                    break;
                }
            }
        }
        if(value === undefined){
            return [false, () => `Expected value ${this.name}`, value]; //TODO I DO NOT GET THIS
        }
        return [passed, fail, value]; //Because coercion
    }

    parse(args, ret = {}, coerce = false){
        if(!(args instanceof Array) && typeof args !== 'string'){
            throw new Error("Args is neither a string nor an array: " + util.inspect(args));
        }else if(!(args instanceof Array)){
            args = args.replace(/\\ /g, '\u000C').replace(/ /g, '\u000B').replace(/\u000C/g, ' ').split('\u000B');
        }
        ret[ParseNode.fail] = ret[ParseNode.fail] || Functional.false;
        ret[ParseNode.ending] = ret[ParseNode.ending] === undefined ? ParseNode.ending : ret[ParseNode.ending];
        if(this.type === ParseNode.types.end){
            ret[ParseNode.fail] = Functional.false;
            ret[ParseNode.ending] = this.ending;
            return true;
        }
        if(this.children.length === 0){
            throw new Error('ParseTree ends without an end-node');
        }
        if((args.length === 0 || !args[0]) && this.type !== ParseNode.types.empty){
            ret[ParseNode.fail] = (u) => {u.tell(`Parameter ${this.name} is missing`)};
            return false;
        }
        let value;
        if(this.type === ParseNode.types.longString){
            value = args.join(' ');
        }else if(this.type === ParseNode.types.string){
            value = args[0].split(' ')[0];
        }else{
            value = args[0];
        }
        let [passed, fail, nvalue] = this.test(value, coerce);
        if(passed){
            ret[this.name] = nvalue;
            for(let child of this.children){
                if(child.parse(args.slice(1, args.length), ret, coerce)){
                    return true;
                }
            }
            return false;
        }else{
            ret[ParseNode.fail] = fail;
            return false;
        }
    }

    /**
     *
     * @returns {{constant: number, user: number, number: number, string: number, longString: number, end: number}}
     */
    static get types(){
        return {
            empty: -1,
            constant: 0,
            user: 1,
            number: 2,
            string: 3,
            longString: 4,
            end: 1000
        }
    }

    static get ending(){
        if(!ParseNode._ending){
            ParseNode._ending = Symbol();
        }
        return ParseNode._ending;
    }

    static get fail(){
        if(!ParseNode._fail){
            ParseNode._fail = Symbol();
        }
        return ParseNode._fail;
    }

    static get _(){
        if(!ParseNode.__){
            ParseNode.__ = Symbol();
        }
        return ParseNode.__;
    }

    static noTree(){
        return new ParseNode(ParseNode.types.end, 'end', {ending: 0});
    }

    static constant(value, name = 'constant'){
        return new ParseNode(ParseNode.types.constant, name, {value: value});
    }

    static anyNumber(name){
        return new ParseNode(ParseNode.types.number, name);
    }

    static positiveNumber(name){
        return new ParseNode(ParseNode.types.number, name, {constraints: [
            new Constraint((n) => n > 0, (u) => u.tell(S.sarcasticFunny))
        ]});
    }

    static user(name = 'user'){
        return new ParseNode(ParseNode.types.user, name, {constraints: [Constraint.notBanned()]});
    }

    static userGrant(name = 'user'){
        return new ParseNode(ParseNode.types.user, name, {
            constraints: [Constraint.notBanned(), new Constraint(
                (u) => u.subscriptions.grant && !u.subscriptions.ultrasilence,
                () => "That user would rather not be granted anything"
            ), new Constraint(
                (u) => u.id !== stclient.steamID, () => "I don't think so"
            )]
        });
    }

    static anyUser(name = 'user'){
        return new ParseNode(ParseNode.types.user, name);
    }

    static singleUser(ending = 0, name = 'user'){
        let ret = ParseNode.user(name).addChild(ParseNode.endNode(ending));
        return ret;
    }

    static anySingleUser(ending = 0, name = 'user'){
        let ret = ParseNode.anyUser(name).addChild(ParseNode.endNode(ending));
        return ret;
    }

    static string(ending = 0, name = 'string'){
        let ret = new ParseNode(ParseNode.types.string, name).addChild(ParseNode.endNode(ending));
        return ret;
    }

    static longString(ending = 0, name = 'longString'){
        let ret = new ParseNode(ParseNode.types.longString, name).addChild(ParseNode.endNode(ending));
        return ret;
    }

    static emptyNode(ending = 0){
        let ret = new ParseNode(ParseNode.types.empty, 'empty').addChild(ParseNode.endNode(ending));
        return ret;
    }

    static endNode(ending = 0){
        return new ParseNode(ParseNode.types.end, 'end', {ending: ending});
    }
}

/**
 * @callback command
 * @param {Object} obj
 * @param {User} user
 * @param {String} place
 */

/**
 * @callback find
 * @param {String} message
 * @param {User} user
 * @param {String} place
 */

class Command {
    /**
     *
     * @param {command} funct
     * @param {ParseNode} startNode
     * @param {Number} availability
     * @param {Constraint[]} constraints
     */
    constructor(funct, startNode, availability, constraints){
        this.funct = funct;
        this.startNode = startNode;
        this.availability = availability;
        this.constraints = constraints;
        this.doc = {};
    }

    isAvailable(place){
        return (place & this.availability) === place;
    }

    /**
     *
     * @param {User} user
     */
    test(user){
        for(let constraint of this.constraints){
            if(!constraint.test(user)){
                return [false, constraint.fail];
            }
        }
        return [true, null];
    }

    /**
     * @typedef {Object} documentObject
     * @property {String} <number> Help for result #<number>
     * @property {String} general General help for the command
     */

    /**
     * Documents a command for use with !help
     * @param {documentObject} doc
     */
    document(doc){
        this.doc = doc;
    }

    static get group(){
        return 0b1;
    }

    static get private(){
        return 0b10;
    }

    static get guivan(){
        return 0b100;
    }
}

class Commander {
    constructor(){
        /** @type {Object.<string, Command>}*/
        this.groupCommands = {};
        /** @type {Object.<string, Command>}*/
        this.guivanCommands = {};
        /** @type {Object.<string, Command>}*/
        this.privateCommands = {}; //For PM, not actually private
        this.emotes = new Persistent('emotes.txt');
        /** @type {Object.<string, function>}*/
        this.finds = {};
    }

    /**
     * Adds a command to the commander
     * @param {string} name The name of the command. Will be changed to "!name" or "Name" depending
     * on where it is available
     * @param {ParseNode} node The starting ParseNode for the command
     * @param {command} funct The function to be executed when the command is executed
     * @param {number} availability Where the command is available
     * @param {Constraint[]} permissions Requirements the user has to fulfill to run this command
     * @returns {Command} The command created
     */
    addCommand(name, node, funct, availability = Command.group, permissions = []){
        if(typeof name !== 'string'){
            throw new Error('Command needs a string name: ' + name);
        }
        if(!(node instanceof ParseNode) && !(node instanceof Array)){
            throw new Error('Node is not a valid node: ' + util.inspect(node));
        }
        let startNode = new ParseNode(ParseNode.types.string, 'command');
        if(node instanceof Array){
            for(let n of node){
                startNode.addChild(n);
            }
        }else{
            startNode.addChild(node);
        }
        if(!(funct instanceof Function)){
            throw new Error('Command needs a valid function: ' + util.inspect(funct));
        }
        if(!(permissions instanceof Array)){
            permissions = [permissions];
        }
        let command = new Command(funct, startNode, availability, permissions);
        name = name.replace(/^\!+/, '').toLowerCase();
        if(command.isAvailable(Command.group)){
            this.groupCommands[Commander.toGroupCommandName(name)] = command;
        }
        if(command.isAvailable(Command.private)){
            this.privateCommands[Commander.toPrivateCommandName(name)] = command;
        }
        if(command.isAvailable(Command.guivan)){
            this.guivanCommands[Commander.toPrivateCommandName(name)] = command;
        }
        return command;
    }

    alias(command, as){
        for(let bag of [this.groupCommands, this.privateCommands, this.guivanCommands]){
            let groupName = '!' + command;
            let privateName = command.charAt(0).toUpperCase() + command.substring(1);
            if(bag.hasOwnProperty(groupName)){
                bag['!' + as] = bag[groupName];
            }else if(bag.hasOwnProperty(privateName)){
                bag[as.charAt(0).toUpperCase() + as.substring(1)] = bag[privateName];
            }
        }
    }

    get groupCommandNames(){
        if(!this._groupCommandNames){
            let arr = [];
            for(let name in this.groupCommands){
                if(this.groupCommands.hasOwnProperty(name)){
                    arr.push(name);
                }
            }
            this._groupCommandNames = arr;
        }
        return this._groupCommandNames;
    }

    get privateCommandNames(){
        if(!this._privateCommandNames){
            let arr = [];
            for(let name in this.privateCommands){
                if(this.privateCommands.hasOwnProperty(name)){
                    arr.push(name);
                }
            }
            this._privateCommandNames = arr;
        }
        return this._privateCommandNames;
    }

    get guivanCommandNames(){
        if(!this._guivanCommandNames){
            let arr = [];
            for(let name in this.guivanCommands){
                if(this.guivanCommands.hasOwnProperty(name)){
                    arr.push(name);
                }
            }
            this._guivanCommandNames = arr;
        }
        return this._guivanCommandNames;
    }

    addEmote(name, emote){
        emote = emote.replace(/\ |\n/g, '');
        name = name.replace(/\ |\n/g, '');
        this.emotes[name] = emote;
        this.emotes.save();
    }

    /**
     *
     * @param {String|String[]} find
     * @param {find} funct
     * @param {Boolean} attentionRequired
     */
    addFind(find, funct, attentionRequired = false){
        if(attentionRequired){
            let prevFunct = funct;
            funct = function(a, user, c){
                if(user.attention || bot.botData.named){
                    prevFunct(a, user, c);
                    user.attention = false;
                    bot.botData.named = false;
                }
            };
        }
        if(find instanceof Array){
            for(let val of find){
                this.finds[val] = funct;
            }
        }else{
            this.finds[find] = funct;
        }
    }

    parse(string, user, place, coerce = false){
        let words = string.split(/\ |\n/);
        let bag = {};
        if(isGroupID(place)){
            bag = this.groupCommands;
        }else if(isUserID(place)){
            bag = this.privateCommands;
        }else if(place === Commander.console){
            bag = this.guivanCommands;
            place = beta ? secrets.steam.betaChat : secrets.steam.mainChat;
        }
        if(bag.hasOwnProperty(words[0])){
            /* Execute before the command */
            if(user.lastVersion !== GOVAN_VERSION){
                let last = user.lastVersion;
                user.lastVersion = GOVAN_VERSION;
                if(!last){
                    user.tell(`Yo! Use !help to get help about my inner workings. Have fun!`);
                }else{
                    user.tell(`:D\n\nIt seems I changed versions since you last used me. Check the !help for more info`);
                }
            }
            user.attention = false;
            bot.botData.named = false;
            /* Command */
            let [pass, fail] = bag[words[0]].test(user);
            if(!pass){ //TODO Unrepeat this code
                let str = fail(user, place);
                if(str){
                    if(bag === this.guivanCommands){
                        str = colors.green(str);
                    }
                    user.tell(str);
                }
            }else{
                let ret = {};
                bag[words[0]].startNode.parse(string, ret, coerce);
                if(ret[ParseNode.ending] !== ParseNode.ending){
                    bag[words[0]].funct(ret, user, place);
                }else{
                    let str = ret[ParseNode.fail](user, place);
                    if(str){
                        if(bag === this.guivanCommands){
                            str = colors.green(str);
                        }
                        user.tell(str);
                    }
                }
            }
        }
        if(/(\:|ː).+(\:|ː)/.test(string)){
            string = string.replace(/ː/g, ':');
            for(let char = string.indexOf(':'); char < string.length; char++){
                let enclosed = string.substring(char + 1, string.indexOf(':', char + 1));//findEnclosing(string, char, ':', ':');
                if(!enclosed){
                    break;
                }
                if(this.emotes.obj.hasOwnProperty(enclosed)){
                    bot.sendMessage(place, this.emotes[enclosed]);
                    return; //This function is done
                }else{
                    char = string.indexOf(':', char + 1 + enclosed.length + 1) - 1; //Redundant to make clear
                    if(char < 0){
                        break;
                    }
                }
            }
        }
        let lstring = string.toLowerCase();
        for(let value in this.finds){
            if(this.finds.hasOwnProperty(value)){
                let regex = new RegExp(`(?:^|[ \\.\\,\\?\\!])${escapeRegex(value)}(?:$|[ \\.\\,\\?\\!])`);
                if(regex.test(lstring)){
                    this.finds[value](string, user, place);
                    break;
                }
            }
        }
    }

    /**
     * Generates a help object for a command
     * @param command The name of the command for which to generate help
     * @returns {String[][][]} First level is the command end, second is a command branch, third is the command elements
     */
    generateHelp(command){
        let startNode = null;
        if(this.groupCommands.hasOwnProperty(command)){
            startNode = this.groupCommands[command].startNode;
        }else if(this.privateCommands.hasOwnProperty(command)){
            startNode = this.privateCommands[command].startNode;
        }
        if(startNode === null){
            return '';
        }

        //Oh boy
        /** @type {ParseNode[]}*/
        let currentPath = [];
        /** @type {Array.<Array.<Array.<String>>>}*/
        let cases = [];
        (function depthFirst(current){
            if(isNumber(current.ending)){
                if(!cases[current.ending]){
                    cases[current.ending] = [];
                }
                let curCase = []; //cases[current.ending];
                for(let i = 0; i < currentPath.length; i++){
                    if(i === 0){
                        curCase.push(command);
                    }else if(typeof currentPath[i] === 'string'){
                        curCase.push(currentPath[i]);
                    }else if(currentPath[i].type === ParseNode.types.constant){
                        curCase.push(`${currentPath[i].name}:`);
                        curCase.push(`"${currentPath[i].value}"`);
                    }else{
                        curCase.push(`${currentPath[i].name}:`);
                        curCase.push(Commander.getName(currentPath[i].type));
                    }
                }
                cases[current.ending].push(curCase);
            }else{
                currentPath.push(current);
                for(let child of current.children){
                    if(!currentPath.includes(child)){
                        depthFirst(child);
                    }else{
                        currentPath.splice(currentPath.indexOf(child), 0, '(');
                        currentPath.push(')*');
                    }
                }
                let pop = currentPath.pop();
                while(typeof pop === 'string'){
                    let count = 0;
                    for(let i = currentPath.length - 1; i >= 0; i--){
                        if(currentPath[i] === ')*'){
                            count++;
                        }else if(currentPath[i] === '('){
                            count--;
                            if(count < 0){
                                currentPath.splice(i, 1);
                                break;
                            }
                        }
                    }
                    pop = currentPath.pop();
                }
            }
        })(startNode);

        function merge(a, b){
            if(a.length !== b.length){
                return false;
            }
            let c = [];
            for(let i = 0; i < a.length; i++){
                if(a[i][a[i].length - 1] === ':' && a[i] !== b[i]){
                    return false;
                }
                c[i] = a[i] === b[i] ? a[i] : `${a[i]}/${b[i]}`;
            }
            return c;
        }

        for(let ending of cases){
            for(let i = 0; i < ending.length - 1; i++){
                let n = merge(ending[i], ending[i+1]);
                if(n){
                    ending.splice(i, 2, n);
                    i--;
                }
            }
        }

        return cases;
    }

    static get console(){
        if(!Commander._console){
            Commander._console = Symbol();
        }
        return Commander._console;
    }

    static getName(type){
        switch(type){
            case ParseNode.types.end:
                return '';
            case ParseNode.types.empty:
                return '';
            case ParseNode.types.longString:
                return 'String';
            case ParseNode.types.number:
                return 'Number';
            case ParseNode.types.string:
                return 'Single word';
            case ParseNode.types.user:
                return 'UserID or username';
        }
    }

    static toGroupCommandName(string){
        if(string.charAt(0) === '!'){
            string = string.slice(1);
        }
        return '!' + string;
    }

    static toPrivateCommandName(string){
        if(string.charAt(0) === '!'){
            string = string.slice(1);
        }
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
}

//endregion

//region Socket.io

class Server {
    constructor(){
        this.port = beta ? 1235 : 1234;
        this.io = new server(this.port);
        /** @type {Object.<number, GovanSocket>}*/
        this.sockets = {};
        /** @type {ConsoleUser[]|Persistent}*/
        this.users = new Persistent('ioUsers.dat', []); //Plain text passwords, I know. But it fits

        let self = this;

        this.io.sockets.on('connection', function(socket){
            self.sockets[socket.id] = new GovanSocket(socket);
            if(socket.handshake.query.version){
                let version = Number.parseFloat(socket.handshake.query.version);
                if(version < GUI_VERSION){
                    socket.emit('print', {text: "You're using an outdated version of the GUI! Get a new one" +
                    " here: http://sergiovan.me/downloads/guivan_latest.zip"});
                }
            }
            let geo = geoip.lookup(socket.handshake.address);
            if(geo){
                self.sockets[socket.id].country = geo.country || 'AQ';
            }else{
                self.sockets[socket.id].country = 'AQ';
            }
            Logger.console(`Console connected from ${self.sockets[socket.id].country}`);

            socket.on('disconnect', function(reason){
                let name = self.sockets[socket.id].user;
                Logger.console(`${name || 'Console'} disconnected${name ? '' : ` from ${self.sockets[socket.id].country}`}`);
                delete self.sockets[socket.id];
            });

            socket.on('auth', function(data){
                if(data.pass){
                    for(let user of self.users.obj){
                        if(user.pass === data.pass){
                            //Handle duplicate logins from different addresses
                            self.sockets[socket.id].user = user._name;
                            self.sockets[socket.id].userID = user.userID;
                            let token = Server.genToken();
                            self.sockets[socket.id].token = token;
                            socket.emit('auth', {success: true, token: token});
                            Logger.console(`${self.sockets[socket.id].name} logged in!`);
                            return;
                        }
                    }
                }
                socket.emit('auth', {success: false});
                Logger.hidden(`Login attempt from ${self.sockets[socket.id].country}`);
            });

            socket.on('command', function(data){
                if(self.sockets[socket.id].token === null || !data.token || self.sockets[socket.id].token !== data.token){
                    Logger.error(`Unauthorised access from ${socket.handshake.address}`);
                    return;
                }
                if(data.command){
                    Logger.console(`${self.sockets[socket.id].name}: ${data.command}`);
                    comm.parse(`${data.command} ${data.params}`, self.sockets[socket.id], Commander.console);
                }else{
                    Logger.console(`Empty message from ${self.sockets[socket.id].name}`);
                }
            });

            socket.on('error', function(err){
                Logger.error(err);
            });
        });
    }

    broadcast(message){
        for(let socket in this.sockets){
            if(this.sockets.hasOwnProperty(socket)){
                this.sockets[socket].tell(message);
            }
        }
    }

    messageSuperUsers(message){
        for(let socket in this.sockets){
            if(this.sockets.hasOwnProperty(socket) && this.sockets[socket].userID && this.sockets[socket].isSuperUser()){
                this.sockets[socket].tell(message);
            }
        }
    }

    messageAdmin(message){
        for(let socket in this.sockets){
            if(this.sockets.hasOwnProperty(socket) && this.sockets[socket].userID === secrets.steam.ownerID){
                this.sockets[socket].tell(message);
            }
        }
    }

    static genPassword(){
        return 'xxxxxxxxxxxxxxxx'.replace(/x/g, () => {
            return (Math.random() * 32 | 0).toString(32);
        });
    }

    static genToken(){
        return 'xxxxxxxxxxx'.replace(/x/g, () => {
            return (Math.random() * 32 | 0).toString(32);
        });
    }
}

class GovanSocket {
    constructor(socket){
        this.socket = socket;
        this.token = null;
        this.user = '';
        this.country = '';
        this.userID = '';

        return new Proxy(this, {
            get: function(target, name){
                if(target[name]){
                    return target[name];
                }else if(target.userID && users._users.obj.hasOwnProperty(target.userID)){
                    return users[target.userID][name];
                }else{
                    return target[name];
                }
            }
        });
    }

    emit(type, obj){
        this.socket.emit(type, obj);
    }

    tell(message){
        this.emit('print', {text: util.format(message)});
    }

    get name(){
        return this.user || this.country || 'AQ';
    }

}

class ConsoleUser {
    constructor(name, pass, userID){
        this._name = name;
        this.pass = pass;
        this.userID = userID;
    }

    get name(){
        if(users && users[this.userID]){
            return users[this.userID].name;
        }else{
            return this._name;
        }
    }

    set name(value){
        this._name = value;
    }
}

//endregion

//endregion

//region Steam Client/Steam User
/* MAGIC AS FUCK, DO NOT TOUCH */
stclient.on('connected', function(){
    Logger.info(`Connected to Steam!`);
    stclient.login();
});

stclient.on('logOnResponse', function(response){
    if(response.eresult === steam.EResult.OK){
        bot.setPersonaState(steam.EPersonaState.Online);
        Logger.status(`Logged on!`);
        if(!beta){
            bot.joinChat(secrets.steam.mainChat);
        }else{
            bot.joinChat(secrets.steam.betaChat);
        }
    }else if(response.eresult === steam.EResult.AccountLogonDenied){
        stclient.disconnect();
        Logger.error('Logon denied, need auth code!');
        exit();
    }else if(response.eresult === steam.EResult.ServiceUnavailable){
        Logger.info('Logon denied, Service unavailable');
    }else{
        stclient.disconnect();
        Logger.error('Logon denied with code ' + response.eresult);
        exit();
    }
});

stclient.on('error', function(){
    Logger.error('Connection closed by server. Thanks Steam');
    users._users.save();
    stclient.connect();
});

stclient.on('loggedOff', function(code){
    Logger.status('Logged off from steam with code ' + code);
});

stuser.on('updateMachineAuth', function(sentry, callback){
    let sentryFile;
    try{
        sentryFile = fs.readFileSync(sentryPath);
    }catch(err){
        sentryFile = null;
    }
    if(!sentryFile) {
        fs.writeFileSync(sentryPath, sentry.bytes);
        callback({sha_file: makeSha(sentry.bytes)});
    }
});
//endregion

//region Bot

bot.on('friend', function(userID, relationship) {
    if (relationship === steam.EFriendRelationship.PendingInvitee) {
        if(bot.personaStates[userID] !== undefined && bot.personaStates[userID].player_name){
            Logger.info(`${bot.personaStates[userID].player_name} tried adding me`);
        }else if(users[userID] !== undefined){
            Logger.info(`${userData[userID].name} tried adding me`);
        }else{
            Logger.info(`${userID} tried adding me`);
        }
        bot.addFriend(userID);
        bot.sendMessage(userID, "You have been added!");
    }
});

bot.on('relationships', function(){
    for(let userID in bot.friends){
        if(bot.friends.hasOwnProperty(userID)){
            if(bot.friends[userID] === steam.EFriendRelationship.RequestRecipient){
                if(bot.personaStates[userID] !== undefined && bot.personaStates[userID].player_name){
                    Logger.info(`${bot.personaStates[userID].player_name} tried adding me`);
                }else if(users[userID] !== undefined){
                    Logger.info(`${users[userID].name} tried adding me`);
                }else{
                    Logger.info(`${userID} tried adding me`);
                }
                bot.addFriend(userID);
                bot.sendMessage(userID, "You have been added!");
            }
        }
    }
});

bot.on('chatEnter', function(chatID, chatResponse){
    Logger.status(`Joined chat`);
    bot.sendMessage(chatID, 'Hello!');
    for(let userID in bot.chatRooms[chatID]){
        if(bot.chatRooms[chatID].hasOwnProperty(userID)){
            users.addUser(userID, bot.personaStates[userID].player_name); //Always exists if in a chat with you
        }
    }
});

bot.on('clanState', function(clanState){
    if(clanState.steamid_clan === bot.botData.chat){
        if(clanState.announcements.length !== 0 || clanState.events.length !== 0){
            let gogogo = false;
            if(clanState.events.length > 0){
                for(let event of clanState.events){
                    gogogo = gogogo || event.just_posted;
                }
            }else{
                gogogo = true;
            }
            if(gogogo){
                bot.sendMessage(bot.botData.chat, "Take cover");
            }
        }
    }
});

bot.on('chatStateChange', function(stateChange, userID, chatID, modID){
    let user = users[userID];
    let log, message;
    if(stateChange === 0x01){ //User joined
        users.addUser(userID, bot.personaStates[userID].player_name);
        user = users[userID];
        log = colors.bgMagenta.white(`${user.name} joined chat`);
        message = user.rpg.bought.welcome;
    }else if(stateChange === 0x02){ //User left
        log = colors.bgMagenta.white(`${user.name} left chat`);
        message = user.rpg.bought.farewell;
        user.lastLeft = now() + 5 * 60 * 1000;
    }else if(stateChange === 0x04){ //User disconnected
        log = colors.bgMagenta.white(`${user.name} died`);
        message = user.rpg.bought.disconnect;
        user.lastLeft = now() + 5 * 60 * 1000;
    }else if(stateChange === 0x08){ //User kicked
        log = colors.bgMagenta.white(`${user.name} got kicked by ${users[modID].name}`);
        message = user.rpg.bought.kick;
        user.lastLeft = now() + 5 * 60 * 1000;
    }else if(stateChange === 0x10){ //User banned
        log = colors.bgMagenta.white(`${user.name} got banned by ${users[modID]}.name`);
        message = user.rpg.bought.ban || "Banhammer'd";
    }else{
        Logger.debug('Undefined state change: ' + stateChange);
        return;
    }
    Logger.log(log);
    if(message && user.subscriptions.get('messages') && (user.lastLeft < now() || stateChange === 0x10)){
        bot.sendMessage(chatID, message);
    }
});

bot.on('chatMsg', function(chatID, message, entryType, userID){
    const user = users[userID];
    let command = message.split(' ')[0];
    let ignore = (user.subscriptions.get('ultrasilence') && message.indexOf('!ultrasilence') !== 0) || user.banned;
    if(comm.groupCommands.hasOwnProperty(command) && !ignore){
        Logger.log(colors.gray(`${user.name} said: ${colors.magenta(command)}${colors.cyan(message.substring(command.length))}`));
    }else{
        Logger.log(colors.gray(`${user.name} said: ${message}`));
    }

    user.lastSpoke = new Date().getTime();

    user.lastLines.pop();
    user.lastLines.unshift(message);

    let ytID = getYoutubeID(message);
    if(ytID){
        youtube.videos.list({
            part: 'snippet',
            id: ytID
        }, function(err, data){
            if(err || !data || !data.items || !data.items[0] || !data.items[0].snippet || !data.items[0].snippet.title){
                Logger.error(err);
                Logger.debug(data);
                Logger.debug(ytID);
                bot.sendMessage(chatID, 'Cannot retrieve title!')
            }else{
                let title = data.items[0].snippet.title;
                bot.sendMessage(chatID, `The title of the video is "${ExternalData.censor(title)}"`);
            }
        });
    }

    let tID = getTwitchID(message);
    if(!ytID && tID){
        let options = {
            url: 'https://api.twitch.tv/kraken/streams/' + tID
        };
        request.get(options, function(err, res, body){
            if(err){
                Logger.error(err);
                bot.sendMessage(chatID, 'Error retrieving channel info');
            }else{
                try{
                    body = JSON.parse(body);
                }catch(anything){
                    Logger.error(body.substring(100), anything);
                    bot.sendMessage(chatID, 'Twitch is being silly...');
                    return;
                }
                if(body.error){
                    Logger.error(body.error);
                    bot.sendMessage("Error retrieving channel info");
                }else{
                    let stream = body.stream;
                    if(stream === null){
                        options = {
                            url: "https://api.twitch.tv/kraken/channels/" + tID
                        };
                        request.get(options, function(err, res, body){
                            if(err){
                                Logger.error(err);
                                bot.sendMessage(chatID, 'Error retrieving channel info');
                            }else{
                                try{
                                    body = JSON.parse(body);
                                }catch(anything){
                                    Logger.error(body.substring(100), anything);
                                    bot.sendMessage(chatID, 'Twitch is being silly...');
                                    return;
                                }
                                if(body.error){
                                    Logger.error(body.error);
                                    bot.sendMessage("Error retrieving channel info");
                                }else{
                                    let cname = body.display_name;
                                    bot.sendMessage(chatID, `${cname}, currently offline`);
                                }
                            }
                        });
                    }else{
                        let cname = body.stream.channel.display_name;
                        let cgame = body.stream.game;
                        let vcount = body.stream.viewers;
                        let vtime = toTimeUnit(now() - Date.parse(body.stream.created_at), 'minutes'); //Minutes since start
                        bot.sendMessage(chatID,
                            `${cname} playing ${cgame} for ${vtime.toFixed(2)} minutes, with ${vcount} viewers`);
                    }
                }
            }
        });
    }

    if(message.toLowerCase().indexOf("prntscr.com") !== -1){
        bot.sendMessage(chatID, "http://getsharex.com");
        return;
    }

    if(ignore){
        //Done
        return;
    }

    bot.botData.named = /(?:^|[\.\,\!\? ])Sir Govan(?:$|[\.\,\!\? ])/.test(message);

    let nums = message.match(/(?:^|[^a-zA-Z])([0-9]+)(?:[^a-zA-Z]|$)/g);
    if(nums && nums.length && randInt(20) < 1){
        if(nums.length <= 5){
            bot.sendMessage(chatID, '10* '.repeat(nums.length).trim());
        }else{
            if(bot.botData.mode === BotData.modes.dalek){
                bot.sendMessage(chatID, 'EXTENMINATE');
            }else{
                bot.sendMessage(chatID, "They're all 10*. Get over it");
            }
        }
    }

    comm.parse(message, user, chatID);

    if(message.length < 140 && !beta && message.indexOf('!') !== 0 && message.indexOf('!') !== 1){
        if(randInt(100) < 1){
            twclient.post('statuses/update', {status: message}, function(err, params, res){
                if(err){
                    Logger.printMessage(colors.bgRed.white('Tweet tweet'), Logger.guivan);
                    Logger.error(err);
                }else{
                    Logger.info('Tweet tweet');
                }
            });
        }
    }

    if(bot.botData.named && !user.attention){
        user.attention = true;
        bot.sendMessage(chatID, `Yes, ${user.rpgname}?`);
    }else if(bot.botData.named && user.attention){
        bot.sendMessage(chatID, `Yes?`);
    }else if(!bot.botData.named && user.attention){
        user.attention = false;
        if(randInt(100) < 2){
            bot.sendMessage(chatID, ExternalData.witty);
        }else{
            bot.sendMessage(chatID, 'What?');
        }
    }
});

bot.on('friendMsg', function(userID, message, entryType){
    if(entryType !== steam.EChatEntryType.ChatMsg){
        return;
    }
    users.addUser(userID, bot.personaStates[userID].player_name);
    let user = users[userID];

    if(message.indexOf('!!') === 0){ //TODO Standarize
        message = message.substring(2);
        user.nextSecret = true;
    }

    if(message.indexOf('Vote') === 0){ //TODO Standarize
        user.nextSuperSecret = true;
    }

    let ignored = (user.subscriptions.get('ultrasilence') && message.indexOf('Ultrasilence') !== 0) || user.banned;
    Logger.printMessage(`${user.nextSuperSecret ? '??????' : users[userID].name} told me: ` +
        `${user.nextSecret && !ignored ? '??????' : message} ${ignored ? ` (Ignored)` : ''}`, Logger.guivan); //Only instance this is used like this

    user.nextSecret = user.nextSuperSecret = false;

    if(ignored){
        return;
    }

    comm.parse(message, user, userID);
});

//endregion

init(); //Let's go