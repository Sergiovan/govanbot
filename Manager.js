var readline = require('readline');
var exec = require('child_process').exec;
var client = require('socket.io-client');

var beta = process.argv.indexOf("-b") !== -1;
var linux = process.argv.indexOf("-l") !== -1;
var cons = process.argv.indexOf("-c") !== -1;
var password = process.argv.indexOf("-p") !== -1 ? process.argv[process.argv.indexOf("-p") + 1] :  null;



var rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

var host = !linux ? "http://localhost:" : "http://bot.sergiovan.me:";
var port = !beta ? "1234" : "1235";
var socket = client.connect(host + port);

var token = null;

console.log(host+port);

socket.on('connect', function(server){
	console.log("Connected to bot");
});

socket.on('disconnect', function(server){
	console.log("Disconnected!");
	token = null;
});

socket.on('command', function(data){
	if(data.command){
		switch(data.command){
			case 'Restart':
				if(beta){
					exec("start .\\ManagerBats\\Bot_Beta.bat");
				}else{
					exec("start .\\ManagerBats\\Bot.bat");
				}
				break;
			default:
				break;
		}
	}
});

socket.on('print', function(data){
	if(cons){
		if(data.text){
			console.log(data.text);
		}
	}
});

socket.on('auth', function(data){
	if(data.success){
		if(data.token){
			token = data.token;
			console.log(time() + "Authorization correct, token recieved!");
		}else{
			console.log(time() + "No token recieved!");
		}
	}else{
		console.log(time() + "Authorization refused!");
	}
});

var pad = function(str, max) {
	str = str.toString();
	return str.length < max ? pad("0" + str, max) : str;
};

var time = function(){
	var date = new Date();
	return pad(date.getDate(),2) + "/" + pad(date.getMonth()+1,2) + "/" + date.getFullYear() + " " + pad(date.getHours(),2) + ":" + pad(date.getMinutes(),2) + ":" + pad(date.getSeconds(),2) + " ";
};

var quest = function(){
	if(cons){
		return;
	}
	var question = "> ";
	if(beta){
		quesition = "beta> ";
	}
	rl.question(question, function(answer){
		var valid = true;
		data = {command: null, params: null, token: null};
		if(token !== null){
			data.token = token;
		}
		if(answer.indexOf("Login") === 0){
			if(token !== null){
				console.log("Already logged in!");
				return;
			}
			if(password !== null){
				socket.emit('auth', {pass: password});
			}else{
				console.log("Yep");
				socket.emit('auth', {pass: answer.substring(6, answer.length)});
			}
			valid = false;
		}else if(answer === 'Start'){ //Rework
			if(beta){
				exec("start .\\ManagerBats\\Bot_Beta.bat");
			}else{
				exec("start .\\ManagerBats\\Bot.bat");
			}
			valid = false;
		}else{
			let arr = answer.split(' ');
			data.command = arr.splice(0, 1)[0];
			data.params = arr.join(' ');
		}
		if(token === null && answer.indexOf("Login") !== 0 && answer.indexOf("Start") !== 0){
			valid = false;
			console.log(time() + "You did not log in yet!");
		}
		if(valid){
			socket.emit('command', data);
		}
		quest();
	});
};

quest();